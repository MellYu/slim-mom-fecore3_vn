export const lightTheme = {
  colors: {
    danger: "#d95d7b",
    primary: "#FC842D",
    primaryShade: "#FBF7F3",
    secondary: "#9B9FAA",
    secondaryShade: "#E0E0E0",
    secondaryShade2: "#F0F1F3",
    secondaryShade3: "#EFF1F3",
    dark: "#264061",
    darkShade: "#212121",
    background: "#ffffff",
    gradient: {
      visible: "rgba(255,255,255,1)",
      unvisible: "rgba(255,255,255,0.5)",
    },
    burger: {
      background: "#264061",
    },
  },
  calendar: {
    background: {
      default: "#FBF7F3",
      hover: "#9B9FAA",
      focus: "#FC842D",
    },
    color: {
      default: "#212121",
      hover: "#FBF7F3",
      focus: "#ffffff",
    },
  },
};

export const darkTheme = {
  colors: {
    danger: "#d95d7b",
    primary: "#33CCCC",
    primaryShade: "#CCFFFF",
    secondary: "#99CCFF",
    secondaryShade: "#003366",
    secondaryShade2: "#003399",
    secondaryShade3: "#006699",
    dark: "#ffffff",
    darkShade: "#99CCCC",
    background: "#000033",
    gradient: {
      visible: "rgba(0,0,51,1)",
      unvisible: "rgba(0,0,51,0.5)",
    },
    burger: {
      background: "#264061",
    },
  },
  calendar: {
    background: {
      default: "#003366",
      hover: "#006699",
      focus: "#003399",
    },
    color: {
      default: "#33CCCC",
      hover: "#CCFFFF",
      focus: "#99CCFF",
    },
  },
};
