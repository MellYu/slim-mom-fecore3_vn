import styled, { useTheme } from "styled-components";
import Hamburger from "hamburger-react";

const WrapperForMargin = styled.div`
  margin-right: -8px;
`;

export const BurgerIcon = ({ burgerToggle, setBurgerToggle }) => {
  const theme = useTheme()
  return (
    <WrapperForMargin>
      <Hamburger color={theme.colors.darkShade} toggled={burgerToggle} toggle={setBurgerToggle} size={20} />
    </WrapperForMargin>
  );
};
