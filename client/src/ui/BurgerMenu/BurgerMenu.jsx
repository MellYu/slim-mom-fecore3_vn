import { NavLink } from "react-router-dom";
import styled from "styled-components";

import {SpringBurger} from "../Animation";

const BurgerMenuStyled = styled.div`
  padding-top: 108px;
  position: absolute;
  bottom: 0;
  width: 100%;
  height: calc(100% - 85px);
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: ${({ theme }) => theme.colors.burger.background};
  z-index: 101;
`;

const Link = styled(NavLink).attrs(({ link, title }) => ({
  to: link,
  children: title,
}))`
  text-decoration: none;
  color: ${({ theme }) => theme.colors.secondary};
  font-size: 24px;
  margin-bottom: 22px;
  font-weight: 700;
  &.active {
    color: ${({ theme }) => theme.colors.background};
    border-bottom: 1px solid ${({ theme }) => theme.colors.background};
  }
  &:hover {
    color: ${({ theme }) => theme.colors.primary};
    text-decoration: none;
  }
`;

export const BurgerMenu = ({ menuList, setBurgerToggle }) => {
  const linkHandlerOnClick = () => {
    setBurgerToggle();
  };
  return (
    <SpringBurger>
        {(props) => (
    <BurgerMenuStyled style={props}>
      {menuList.map((item) => (
        <Link
          title={item.title}
          link={item.link}
          key={item.title}
          onClick={linkHandlerOnClick}
        />
      ))}
    </BurgerMenuStyled>
    )}
    </SpringBurger>
  );
};
