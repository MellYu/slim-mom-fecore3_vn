import React from "react";
import { connect } from "react-redux";

import {
  Modal as ModalRS,
  ModalBody as ModalBodyRS,
  ModalHeader as ModalHeaderRS,
} from "reactstrap";
import styled from "styled-components";
import { checkLoaders } from "../../redux/loaders";
import { LoaderSpiner } from "../";

const ModalWrapper = styled(ModalRS)`
  @media (min-width: 576px) {
    max-width: 100%;
  }

  & .modal-content {
    background-color: ${({ theme }) => theme.colors.secondaryShade};
    width: 680px;
    min-height: 579px;
  }
  display: flex;
  justify-content: center;
  margin-top: 135px;
`;

const ModalHeader = styled(ModalHeaderRS)`
  border: none;
  & button span {
    color: ${({ theme }) => theme.colors.dark};
  }
`;

const ModalBody = styled(ModalBodyRS)`
  margin: 0 "auto";
  padding: 0px;

  @media screen and (min-width: 768px) {
    margin: 0 auto;
    padding: 8px 43px 0px;
  }

  @media screen and (min-width: 1024px) {
    padding: 8px 90px 0px;
  }
`;

const Container = ({ toggle, modal, children, isLoading }) => {
  return (
    <ModalWrapper isOpen={modal} toggle={toggle}>
      {isLoading ? (
        <LoaderSpiner background={false} />
      ) : (
        <>
          <ModalHeader toggle={toggle}></ModalHeader>
          <ModalBody>{children}</ModalBody>
        </>
      )}
    </ModalWrapper>
  );
};

const mapStateToProps = (state) => ({
  isLoading: checkLoaders(state),
});

export const Modal = connect(mapStateToProps)(Container);
