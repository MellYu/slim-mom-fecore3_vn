import React from "react";
import Loader from "react-loader-spinner";
import styled, { useTheme } from "styled-components";

const LoaderWrapper = styled.div`
  position: fixed;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;

const LoaderBackGround = styled.div`
  position: absolute;
  top: 0;
  width: 100%;
  height: 100%;
  background-color: ${({ background, theme }) =>
    background ? theme.colors.background : "inherit"};
`;

export const LoaderSpiner = ({ background }) => {
  const theme = useTheme();

  return (
    <LoaderBackGround background={background}>
      <LoaderWrapper>
        <Loader
          type="ThreeDots"
          color={theme.colors.primary}
          height={120}
          width={120}
          opacity={0.5}
        />
      </LoaderWrapper>
    </LoaderBackGround>
  );
};
