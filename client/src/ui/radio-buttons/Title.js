import styled from "styled-components";

import { P } from "../Fonts";

export const Title = styled(P).attrs(({ children }) => ({
  children: `${children} *`,
}))`
  color: ${({ theme }) => theme.colors.secondary};
`;
