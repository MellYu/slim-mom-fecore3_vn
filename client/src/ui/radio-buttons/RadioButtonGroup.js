import React, { useState } from 'react'

export const RadioButtonGroupContext = React.createContext({})


export const RadioButtonGroup = ({
  onChange,
  children,
  name,
  ...props
}) => {
  const [value, changeValue] = useState(props.value)
 
  return (
    <RadioButtonGroupContext.Provider
      value={{
        name,
        value,
        onChange: (e) => {
          changeValue(e.target.value)
          if (onChange) onChange(e)
        }
      }}
    >
      {children}
    </RadioButtonGroupContext.Provider>
  )
}
