import { FormText as FormTextRS } from "reactstrap";
import styled from "styled-components";

export const FormText = styled(FormTextRS).attrs(({ value }) => ({
  children: value,
}))`
  color: ${({ theme, checked, disabled }) =>
    !checked || disabled
      ? theme.colors.secondary
      : theme.colors.primary} !important;
  font-size: 14px;
  display: inline-block;
`;
