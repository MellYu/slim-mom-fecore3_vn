import React, { useState } from "react";

import { Input, Icon } from "../";
import { Box } from "./Box";
import { FormText } from "./FormText";
import { RadioButtonGroupContext } from "./RadioButtonGroup";

const RadioButtonInputCheckBox = (props) => (
  <>
    <Box {...props}>
      <Icon name="dot" />
    </Box>
    <FormText {...props} />
  </>
);

export const RadioButton = ({
  onChange,
  disabled,
  component: Component = RadioButtonInputCheckBox,
  formDataSet,
  ...props
}) => {
  const radioButtonGroupCtx = React.useContext(RadioButtonGroupContext);
  const [checked, changeChecked] = useState(!!props.checked);

  let state = checked;

  if (typeof props.checked === "boolean") state = props.checked;
  else if (Object.keys(radioButtonGroupCtx).length)
    state = radioButtonGroupCtx.value === props.value;

  const handlerInputOnChamge = (e) => {
    formDataSet(e);
    changeChecked(e.target.checked);
    onChange?.(e);
    radioButtonGroupCtx.onChange?.(e);
  };
  return (
    <>
      <Input
        {...props}
        name={props.name ?? radioButtonGroupCtx.name}
        onChange={handlerInputOnChamge}
        disabled={disabled}
        checked={state}
        placeholder="radio"
        type="radio"
      />
      <Component disabled={disabled} checked={state} value={props.value} />
    </>
  );
};
