import styled from 'styled-components'

import { Icon } from '../'

export const Box = styled.div`
  cursor: ${({ disabled }) => (disabled ? 'default' : 'pointer')};
  border: ${({ theme }) => `1px solid ${theme.colors.secondaryShade}`};
  background-color: ${({ theme, disabled }) =>
    disabled ? theme.colors.secondaryShade : undefined};
  height: 20px;
  width: 20px;
  display: inline-flex;
  justify-content: center;
  align-items: center;
  border-radius: 50%;
  margin-right: 6px;
  &:hover {
    background-color: ${({ theme, disabled }) =>
      disabled ? theme.colors.secondary : theme.colors.secondaryShade};
  }
  ${Icon} * {
    fill: ${({ theme, checked, disabled }) =>
      !checked || disabled ? 'transparent' : theme.colors.primary};
  }
`
