import styled from "styled-components";

import { SelectInput } from "../select";
import { ValueContainer } from "./ValueContainer";
import { Option } from "./Option";

export const SelectInputTextRight = styled(SelectInput).attrs((props) => ({
  ...props,
  components: {
    ...props.components,
    ValueContainer,
    Option
  }
}))``