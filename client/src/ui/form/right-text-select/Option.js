import styled from "styled-components";

import { Option as OptionUI } from "../select";

export const Option = styled(OptionUI)`
  text-align: right;
`
