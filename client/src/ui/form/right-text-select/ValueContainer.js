import styled from "styled-components";

import { ValueContainer as ValueContainerUI } from "../select";

export const ValueContainer = styled(ValueContainerUI)`
  display: flex;
  justify-content: flex-end;
`
