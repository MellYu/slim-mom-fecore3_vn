import React, { useState } from "react";
import { useDispatch } from "react-redux";
import styled from "styled-components";

import { setTheme } from "../../../redux/theme";
import { SelectInput } from "../select/SelectInput";
import { ValueContainer as ValueContainerUI } from "../select/ValueContainer";

const ValueContainer = styled(ValueContainerUI)`
  padding: 0 10px !important;
  display: flex;
`

const options = [
  { value: "lightTheme", label: "Светлая тема" },
  { value: "darkTheme", label: "Темная тема" },
];

export const SelectTheme = styled((props) => {
  const [selectValue, setSelectValue] = useState(
    options.find((option) => localStorage.getItem("theme") === option.value)
  );
  const dispatch = useDispatch();

  const handlerSelectInputOnChange = (selectedOption) => {
    setSelectValue(selectedOption);
    dispatch(setTheme(selectedOption.value));
    localStorage.setItem("theme", selectedOption.value);
  };

  return (
        <SelectInput
          {...props}
          components={{ValueContainer}}
          options={options}
          value={selectValue}
          onChange={handlerSelectInputOnChange}
        />
  );
})`
  width: 130px;
`;
