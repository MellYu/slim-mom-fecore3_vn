import styled from "styled-components";

import { SelectInput } from "../select";
import { MenuList } from "./MenuList";

export const VirtualizedSelect = styled(SelectInput).attrs((props) => ({
  ...props,
  components: {
    ...props.components,
    MenuList,
  },
}))``;
