import { List } from "react-virtualized";
import styled from "styled-components";

export const MenuList = styled(({ children, className }) => {
  const rows = children;
  const rowRenderer = ({ key, index, style }) => {
    return (
      <div key={key} style={style}>
        {rows[index]}
      </div>
    );
  };

  return (
    <List
      className={className}
      style={{ width: "100%" }}
      width={300}
      height={300}
      rowHeight={30}
      rowCount={rows.length !== undefined ? rows.length : 0}
      rowRenderer={rowRenderer}
    />
  );
})`
  border-radius: 4px;
  min-height: auto;
  max-height: 300px;
  height: auto !important;
  background-color: ${({ theme }) => theme.colors.background};
  border: 1px solid ${({ theme }) => theme.colors.secondaryShade};
`;
