import { components } from 'react-select'
import styled from 'styled-components'

export const MenuList = styled(components.MenuList)`
  padding: 0 !important;
  border-radius: 4px;
  border: 1px solid ${({ theme }) => theme.colors.secondaryShade};
`
