import { components } from 'react-select'
import styled from 'styled-components'

export const ValueContainer = styled(components.ValueContainer)`
  font-weight: 700;
  box-shadow: none;
  background-image: none;
  padding: 20px 0 !important;
  color: ${({ theme }) => theme.colors.secondary};
  border-bottom: 1px solid ${({ theme }) => theme.colors.secondaryShade};
  &:hover {
    border-color: ${({ theme }) => theme.colors.secondaryShade};
  }
  &:disabled {
    border-color: ${({ theme }) => theme.colors.secondaryShade};
    &::placeholder {
      color: ${({ theme }) => theme.colors.secondaryShade};
    }
  }
  &:not(:disabled):not(.disabled):focus {
    color: ${({ theme }) => theme.colors.tertiary};
    box-shadow: none;
    border-color: ${({ theme }) => theme.colors.secondaryShade};
  }
`
