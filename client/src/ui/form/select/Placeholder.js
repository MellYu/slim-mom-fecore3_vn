import { components } from 'react-select'
import styled from 'styled-components'

export const Placeholder = styled(components.Placeholder)`
  color: ${({ theme }) => theme.colors.secondary};
`