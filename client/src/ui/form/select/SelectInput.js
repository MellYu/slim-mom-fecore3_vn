import styled from "styled-components";
import Select from "react-select";

import { Option } from "./Option";
import { ValueContainer } from "./ValueContainer";
import { MenuList } from "./MenuList";
import { Placeholder } from "./Placeholder";

export const SelectInput = styled(Select).attrs((props) => ({
  isSearchable: true,
  ...props,
  components: {
    DropdownIndicator: null,
    IndicatorSeparator: null,
    Option,
    ValueContainer,
    MenuList,
    Placeholder,
    ...props.components,
  },
}))`
  & > div {
    box-shadow: none;
    border: none !important;
  }
`;
