import { components } from 'react-select'
import styled from 'styled-components'

export const Option = styled(components.Option)`
  font-weight: 500;
  padding: 12px 17px 14px 17px !important;
  color: ${({ theme, isSelected, isDisabled }) =>
    isSelected
      ? theme.colors.primary
      : isDisabled
      ? theme.colors.secondaryShade
      : theme.colors.secondary} !important;
  background-color: ${({ theme }) => theme.colors.background} !important;
  &:hover {
    background-color: ${({ theme, isDisabled }) =>
      isDisabled ? theme.colors.secondary : theme.colors.primaryShade} !important;
  }
`
