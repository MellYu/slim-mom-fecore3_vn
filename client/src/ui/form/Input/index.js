import styled from "styled-components";
import InputRs from "reactstrap/es/Input";

export const Input = styled(InputRs).attrs(({ placeholder }) => ({
  placeholder: placeholder !== undefined && `${placeholder} *`,
}))`
  &:not([type="radio"]) {
    outline: none;
    border-radius: 4px;
    height: 56px;
    border: none;
    font-weight: 700;
    width: 239px;
    background-color: inherit;
    border-bottom: 1px solid
      ${({ invalid, theme }) =>
        invalid ? theme.colors.danger : theme.colors.secondaryShade};
    color: ${({ theme }) => theme.colors.dark};
    padding: 0 5px;
    &::placeholder {
      color: ${({ invalid, theme }) =>
        invalid ? theme.colors.danger : theme.colors.secondary};
    }
    &:not(:disabled):not(.disabled):hover {
      box-shadow: 0 0 1px ${({ theme }) => theme.colors.secondaryShade};
      background: linear-gradient(
        0deg,
        rgba(0, 0, 0, 0.05),
        rgba(0, 0, 0, 0) 20%
      );
      &:disabled {
        background-color: inherit;
        border-color: ${({ theme }) => theme.colors.secondaryShade};
        &::placeholder {
          color: ${({ theme }) => theme.colors.secondary};
        }
      }
    }
    &:not(:disabled):not(.disabled):focus {
      color: ${({ theme }) => theme.colors.secondary};
      background: linear-gradient(
        0deg,
        rgba(0, 0, 0, 0.05),
        rgba(0, 0, 0, 0) 30%
      );
      background-color: inherit;
      border-color: ${({ invalid, theme }) =>
        invalid ? theme.colors.danger : theme.colors.secondaryShade};
      box-shadow: none;
    }
  }
  &[type="radio"] {
    visibility: hidden;
  }
`;
