import styled from "styled-components";

import LabelRs from "reactstrap/es/Label";

export const Label = styled(LabelRs)`
  cursor: ${({ disabled }) => (disabled ? "default" : "pointer")};
  color: ${({ theme }) => theme.colors.secondary};
  margin: 0;
`;
