import React from "react";
import { Redirect, Route } from "react-router-dom";
import { connect } from "react-redux";

import { getUserisAuthenticate } from "../redux/auth/selectors";

const Container = ({ component, redirect, isAuthenticate, ...props }) => {
  return isAuthenticate ? (
    <Redirect to={redirect} />
  ) : (
    <Route {...props}>{component}</Route>
  );
};

const mapStateToProps = (state) => ({
  isAuthenticate: getUserisAuthenticate(state),
});

export const RestrictedRoute = connect(mapStateToProps)(Container);
