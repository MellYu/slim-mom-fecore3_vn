import styled from 'styled-components'

import * as icons from './assets'

export const Icon = styled.svg.attrs(({ name }) => ({
  as: icons[name]
}))`
  path {
    fill: ${({ color, theme }) => (color ? theme.colors[color] : undefined)};
  }
`
