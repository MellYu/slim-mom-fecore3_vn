export { ReactComponent as add } from './add.svg'
export { ReactComponent as calendar } from './calendar.svg'
export { ReactComponent as dot } from './dot.svg'
export { ReactComponent as close } from './close.svg'
export { ReactComponent as burger } from './burger.svg'
export { ReactComponent as goBackArrow } from './goBackArrow.svg'
