import React, { useEffect, useState } from "react";
import { Calendar as CalendarRC } from "react-calendar";
import "react-calendar/dist/Calendar.css";
import styled from "styled-components";

import { CalendarButton } from "../buttons";
import { SpringCalendar } from "../Animation";

const WrapperCalendar = styled.div`
  position: absolute;
  z-index: 10;
  top: 40px;
  & .react-calendar__navigation button {
    color: ${({ theme }) => theme.calendar.color.default};
    &:hover {
      color: ${({ theme }) => theme.calendar.color.hover};
      background-color: ${({ theme }) => theme.calendar.background.hover};
    }
  }
  & .react-calendar__month-view__weekdays__weekday {
    & abbr {
      color: ${({ theme }) => theme.calendar.color.default};
    }
  }
  & .react-calendar__month-view__days__day {
    color: ${({ theme }) => theme.calendar.color.default};
    &:hover {
      color: ${({ theme }) => theme.calendar.color.hover};
      background-color: ${({ theme }) => theme.calendar.background.hover};
    }
  }
  & .react-calendar__tile--now {
    background-color: ${({ theme }) => theme.colors.secondaryShade};
  }
  & .react-calendar {
    background-color: ${({ theme }) => theme.calendar.background.default};
    color: ${({ theme }) => theme.calendar.color.default};
  }
  & .react-calendar__tile--active,
  .react-calendar__tile--active:enabled:focus {
    color: ${({ theme }) => theme.calendar.color.default};
    background: ${({ theme }) => theme.calendar.background.focus};
  }
`;
const Wrapper = styled.div`
  position: relative;
`;

export const Calendar = ({ className, setDate }) => {
  const [open, setOpen] = useState(false);

  const formatedDate = (e) => {
    const formatedDate =
      ("0" + e.getDate()).slice(-2) +
      "-" +
      ("0" + (e.getMonth() + 1)).slice(-2) +
      "-" +
      e.getFullYear();
    return formatedDate;
  };

  const [value, setValue] = useState(new Date());
  useEffect(() => {
    setDate(formatedDate(value));
  }, [open]);
  const calendarBtnHandlerOnClick = () => {
    setOpen(!open);
  };
  const calendarRCHandlerOnChange = (e) => {
    setValue(e);
    setDate(formatedDate(e));
    setOpen(!open);
  };
  return (
    <Wrapper>
      <CalendarButton onClick={calendarBtnHandlerOnClick} />
      {open && (
        <SpringCalendar toggle={open}>
          {(props) => (
            <WrapperCalendar style={props} className={className}>
              <CalendarRC
                defaultValue={value}
                onChange={calendarRCHandlerOnChange}
              />
            </WrapperCalendar>
          )}
        </SpringCalendar>
      )}
    </Wrapper>
  );
};
