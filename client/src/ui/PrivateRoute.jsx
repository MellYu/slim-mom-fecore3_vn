import React from "react";
import { Redirect, Route } from "react-router-dom";
import { connect } from "react-redux";

import { getUserisAuthenticate } from "../redux/auth";
import routes from "../routes";

const Container = ({ component, isAuthenticate, ...props }) => {
  return !isAuthenticate ? (
    <Redirect to={routes.home} />
  ) : (
    <Route {...props}>{component}</Route>
  );
};

const mapStateToProps = (state) => ({
  isAuthenticate: getUserisAuthenticate(state),
});

export const PrivateRoute = connect(mapStateToProps)(Container);
