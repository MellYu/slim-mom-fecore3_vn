import { Button } from "reactstrap";
import styled from "styled-components";

import { Icon } from "../../Icon";

export const CloseButton = styled(Button).attrs(() => ({
  children: <Icon name="close" />,
  color: "link",
}))`
  background-color: inherit;
  border: none;
  outline: none;
  &:hover {
    background-color: inherit;
    border: none;
    box-shadow: none;
    & svg path {
      stroke: ${({ theme }) => theme.colors.primary};
    }
  }
  &:active {
    background-color: inherit;
    border: none;
    box-shadow: none;
  }
  &:focus {
    background-color: inherit;
    border: none;
    box-shadow: none;
  }
`;
