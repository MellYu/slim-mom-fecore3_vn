import { Button as ButtonRS } from "reactstrap/es";
import styled from "styled-components";

export const Button = styled(ButtonRS).attrs(() => ({
  color: "link",
}))`
  cursor: pointer;
  width: 176px;
  height: 44px;
  font-weight: 700;
  background-color: ${({ theme, primary }) =>
    primary ? theme.colors.primary : theme.colors.background};
  box-shadow: ${({ theme, primary }) =>
    primary ? `0px 4px 8px ${theme.colors.primary}` : "none"};
  border-radius: 30px;
  border: ${({ theme, primary }) =>
    primary ? "none" : `2px solid ${theme.colors.primary}`};
  font-size: 14px;
  color: ${({ theme, primary }) =>
    primary ? theme.colors.background : theme.colors.primary};
  &:hover {
    box-shadow: none;
    color: ${({ theme }) => theme.colors.primary};
    background-color: ${({ theme }) => theme.colors.primaryShade};
    border: ${({ theme }) => `2px solid ${theme.colors.primary}`};
  }
  &:disabled {
    background-color: ${({ theme }) => theme.colors.secondaryShade2};
    pointer-events: none;
    box-shadow: none;
    border: ${({ theme }) => `2px solid ${theme.colors.secondary}`};
    color: ${({ theme }) => theme.colors.secondary};
  }
`;
