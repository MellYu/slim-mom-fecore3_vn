import { Button } from "reactstrap";
import styled from "styled-components";
import { Icon } from "../../Icon";

export const CalendarButton = styled(Button).attrs(() => ({
  children: <Icon name="calendar" />,
  color: "link",
}))`
  background-color: inherit;
  padding:0;
  border: none;
  outline: none;
  &:hover {
    background-color: inherit;
    border: none;
    box-shadow: none;
    & svg path {
      fill: ${({ theme }) => theme.colors.primary};
    }
  }
  &:active {
    background-color: inherit;
    border: none;
    box-shadow: none;
  }
  &:focus {
    background-color: inherit;
    border: none;
    box-shadow: none;
  }
`;
