import { Button as ButtonRS } from "reactstrap/es";
import styled from "styled-components";
import { Icon } from "../../Icon";

export const GoBackButton = styled(ButtonRS).attrs(() => ({
  children: <Icon name="goBackArrow" />,
  color: "link",
}))`
  outline: none;
  border: none;
  padding: 0;
  & svg path {
    stroke: ${({ theme }) => theme.colors.darkShade};
  }
  background-color: ${({ theme }) => theme.colors.secondaryShade3} !important;
  &:active, &:focus{
    box-shadow: none;
    border: none;
  }
`;
