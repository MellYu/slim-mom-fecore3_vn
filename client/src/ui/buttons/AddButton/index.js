import { Button } from "reactstrap/es";
import styled from "styled-components";
import { Icon } from "../../Icon";

export const AddButton = styled(Button).attrs(() => ({
  children: <Icon name="add" />,
  color: "link",
}))`
  cursor: pointer;
  outline: none;
  width: 50px;
  height: 50px;
  background-color: ${({ theme }) => theme.colors.primary};
  box-shadow: ${({ theme }) => `0px 4px 8px ${theme.colors.primary}`};
  border-radius: 50%;
  border: none;
  &:hover {
    box-shadow: ${({ theme }) => `0 0 8px ${theme.colors.primary}`};
    border: 1px solid ${({ theme }) => theme.colors.primary};
    & svg path {
      fill: ${({ theme }) => theme.colors.primary};
    }
    background-color: ${({ theme }) => theme.colors.background};
  }
  &:disabled {
    background-color: ${({ theme }) => theme.colors.secondaryShade2};
    pointer-events: none;
    box-shadow: none;
    border: ${({ theme }) => `2px solid ${theme.colors.secondary}`};
    & svg path {
      fill: ${({ theme }) => theme.colors.secondary};
    }
  }
  &:focus {
    background-color: ${({ theme }) => theme.colors.primary};
    box-shadow: ${({ theme }) => `0px 4px 8px ${theme.colors.primary}`};
    border: 1px solid ${({ theme }) => theme.colors.primary};
    & svg path {
      fill: ${({ theme }) => theme.colors.primaryShade};
    }
  }
`;
