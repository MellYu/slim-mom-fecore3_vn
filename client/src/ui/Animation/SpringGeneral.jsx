import { Spring } from "react-spring/renderprops";

export const SpringGeneral = ({ children }) => (
  <Spring
    from={{ opacity: 0 }}
    to={{ opacity: 1 }}
    config={{ delay: 500, duration: 500 }}
  >
    {children}
  </Spring>
);
