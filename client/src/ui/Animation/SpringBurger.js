import { Spring } from "react-spring/renderprops";

export const SpringBurger = ({ children }) => {
  return (
    <Spring
      from={{ opacity: 0.5, transform: "translate(100%,0)" }}
      to={{ opacity: 1, transform: "translate(0%,0)" }}
      config={{ duration: 200 }}
    >
      {children}
    </Spring>
  );
};
