import { Spring } from "react-spring/renderprops";

export const AppearanceOnLeft = ({ children }) => (
  <Spring
    from={{ opacity: 0, transform: "translate(-100%,0)" }}
    to={{ opacity: 1, transform: "translate(0%,0)" }}
    config={{ duration: 500 }}
  >
    {children}
  </Spring>
);
