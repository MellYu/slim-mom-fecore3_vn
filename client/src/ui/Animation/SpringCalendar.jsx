import { Spring } from "react-spring/renderprops";

export const SpringCalendar = ({ children, toggle}) => (
  <Spring
    from={{ opacity: 0, transform: "translateY(4rem)" }}
    to={{ opacity: 1, transform: "translateY(0rem)" }}
    config={{ delay: 200, duration: 200 }}
  >
    {children}
  </Spring>
);
