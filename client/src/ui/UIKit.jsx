import React, { useState } from "react";
import { Col, Container, Row } from "reactstrap";
import styled from "styled-components";

import { RadioButton, RadioButtonGroup, RadioButtonTitle } from ".";
import { Button as ButtonUI, AddButton } from "./buttons";
import { Calendar as CalendarUI } from "./Calendar";
import { H2 as H2UI } from "./Fonts";
import { Input, SelectInput, SelectInputTextRight } from "./form";
import { Icon as IconUI } from "./Icon";
import { Label } from "./Label";
import { LoaderSpiner } from "./Loader";

const H2 = styled(H2UI)`
  margin: 20px 0;
`;

const Calendar = styled(CalendarUI)`
  left: -100px;
`;

const Button = styled(ButtonUI)`
  margin: 20px 20px 20px 0;
`;

const Icon = styled(IconUI)`
  margin: 20px;
`;

const options = [
  { value: "chocolate", label: "Chocolate" },
  { value: "strawberry", label: "Strawberry" },
  { value: "vanilla", label: "Vanilla" },
  { value: "chocolate", label: "Chocolate" },
  { value: "strawberry", label: "Strawberry" },
  { value: "vanilla", label: "Vanilla" },
  { value: "chocolate", label: "Chocolate" },
  { value: "strawberry", label: "Strawberry" },
  { value: "vanilla", label: "Vanilla" },
  { value: "chocolate", label: "Chocolate" },
  { value: "strawberry", label: "Strawberry" },
  { value: "vanilla", label: "Vanilla" },
  { value: "chocolate", label: "Chocolate" },
  { value: "strawberry", label: "Strawberry" },
  { value: "vanilla", label: "Vanilla" },
  { value: "chocolate", label: "Chocolate" },
  { value: "strawberry", label: "Strawberry" },
  { value: "vanilla", label: "Vanilla" },
  { value: "chocolate", label: "Chocolate" },
  { value: "strawberry", label: "Strawberry" },
  { value: "vanilla", label: "Vanilla" },
  { value: "chocolate", label: "Chocolate" },
  { value: "strawberry", label: "Strawberry" },
  { value: "vanilla", label: "Vanilla" },
];

export const UIKit = () => {
  const [selectValue, setSelectValue] = useState();
  const [secondarySelectValue, setSecondarySelectValue] = useState();

  return (
    <Container>
      <Row>
        <Col xl={6}>
          <H2>Input</H2>
          <Input placeholder="Enter your text" />
          <H2>Radio Button</H2>
          <RadioButtonTitle>Title</RadioButtonTitle>
          <RadioButtonGroup>
            <Label>
              <RadioButton value="test 1" name="test" />
            </Label>
            <Label>
              <RadioButton value="test 2" name="test" />
            </Label>
          </RadioButtonGroup>
          <H2>Icon Button</H2>
          <AddButton />
          <H2>Calendar</H2>
          {/* <Calendar /> */}
          <LoaderSpiner />
        </Col>
        <Col xl={6}>
          <H2>Button</H2>
          <Button primary={+true}>Primary</Button>
          <Button>Secondary</Button>
          <H2>Select</H2>
          <SelectInput
            options={options}
            value={selectValue}
            placeholder={"Choice..."} defaultValue={options[1]}
            onChange={setSelectValue}
          />
          <SelectInputTextRight
            options={options}
            value={secondarySelectValue}
            placeholder={"Choice..."}
            onChange={setSecondarySelectValue}
          />
        </Col>
      </Row>
      <H2>Icon</H2>
      <Icon name="burger" />
      <Icon name="calendar" />
      <Icon name="close" />
      <Icon name="dot" />
    </Container>
  );
};
