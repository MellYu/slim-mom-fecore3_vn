import styled, { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`
  body {
    font-family: 'IBM Plex Sans', sans-serif !important;
    font-size: 14px;
    font-weight: normal;
    min-height: 100vh;
    background-color: ${({ theme }) => theme.colors.background};
  }
  ul, ol {
    padding: 0;
    margin: 0;
  }
  input, button {
    outline: none;
  }
  li {
    text-decoration: none
  }
  p, h1, h2, h3, h4, h5, h6, hr {
    margin: 0;
  }
  #root {
    position: relative;
    min-height: 100vh;
    min-width: 100vw;
    overflow: hidden;
  }
`;

export const P = styled.p`
  font-size: 14px;
  font-style: normal;
  font-weight: 700;
`;

export const H1 = styled.h1`
  font-weight: 700;
  font-size: 34px;
 
  @media (max-width: 768px) {
    font-size: 30px;
  } ;
  @media (max-width: 380px) {
    font-size: 18px;
  }
`;

export const H2 = styled.h2`
  font-weight: 700;
  font-size: 26px;
  @media (max-width: 380px) {
    font-size: 18px;
  }
`;

export const H3 = styled.h3`
  font-size: 24px;
  font-weight: 700;
  @media (max-width: 380px) {
    font-size: 18px;
  }
`;

export const H4 = styled.h4`
  font-size: 14px;
  font-weight: 700;
  line-height: 17px;
`;

export const CaloriesValue = styled.h3`
  font-size: 48px;
  font-weight: 700;
  line-height: 58px;
`;

export const CaloriesSpan = styled.span`
  font-size: 16px;
  font-weight: 700;
`;
