import { createReducer } from "@reduxjs/toolkit";
import { setUserParameters } from "./action";

const initialState = {
  dailyRate: null,
  notAllowedProducts: null,
  bloodGroup: null,
};

export const userParametersReducer = createReducer(initialState, {
  [setUserParameters]: (state, { payload }) => {
    let fourProduct = payload.notAllowedProducts.map(
      (product) => product.title.ua
    );
    fourProduct.length = 4;
    return {
      ...state,
      dailyRate: payload.dailyRate,
      notAllowedProducts: fourProduct,
      bloodGroup: payload.bloodType,
    };
  },
});
