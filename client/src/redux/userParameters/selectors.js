export const getUserDailyRate = (state) => state.userParameters.dailyRate;
export const getUserBloodGroup = (state) => state.userParameters.bloodGroup;
export const getUserNotAllowedProducts = (state) =>
  state.userParameters.notAllowedProducts;
