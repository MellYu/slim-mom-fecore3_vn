import { setUserParameters } from "./action";
import { setLoaderUserParameters } from "../loaders";
import { API } from "../../api";

export const calculateUserParametersmeters = (payload) => async (dispatch) => {
  try {
    dispatch(setLoaderUserParameters(true));
    const response = await fetch(`${API}/products/calculator`, {
      method: "POST",
      body: JSON.stringify(payload),
      headers: {
        "Content-Type": "application/json",
      },
    });
    const calculateData = await response.json();
    dispatch(setUserParameters(calculateData));
        setTimeout(() => dispatch(setLoaderUserParameters(false)), 5000);
  } catch (error) {
        setTimeout(() => dispatch(setLoaderUserParameters(false)), 5000);
    console.log(error);
  }
};

export const fetchUserParameters = (payload) => async (dispatch) => {
  try {
    dispatch(setLoaderUserParameters(true));
    const response = await fetch(`${API}/users/${payload}`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("accessToken")}`,
      },
    });
    const userParameters = await response.json();
    dispatch(setUserParameters(userParameters));
    if (userParameters.dailyRate !== 0)
          setTimeout(() => dispatch(setLoaderUserParameters(false)), 5000);
  } catch (error) {
        setTimeout(() => dispatch(setLoaderUserParameters(false)), 5000);
    console.log(error);
  }
};

export const setUserParametersOperation = (payload) => async (dispatch) => {
  try {
    const { userDailyRate, userBloodGroup, userId } = payload;
    // eslint-disable-next-line no-unused-vars
    const response = await fetch(`${API}/users/${userId}`, {
      method: "POST",
      body: JSON.stringify({ userDailyRate, userBloodGroup }),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("accessToken")}`,
      },
    });
  } catch (error) {
    console.log(error);
  }
};
