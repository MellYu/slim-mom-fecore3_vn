import { createAction } from "@reduxjs/toolkit";
import { SET_USER_PARAMETERS } from "./types";

export const setUserParameters = createAction(SET_USER_PARAMETERS);
