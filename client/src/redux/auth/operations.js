/* eslint-disable no-unused-vars */
import "@pnotify/core/dist/PNotify.css";
import "@pnotify/core/dist/BrightTheme.css";
import * as PNotifyMobile from "@pnotify/mobile";
import "@pnotify/mobile/dist/PNotifyMobile.css";
import { defaults, error } from "@pnotify/core";
import { info, defaultModules } from "@pnotify/core";

import { API } from "./../../api";
import { setUserData, setUserToken } from "./actions";
import { setUserParameters } from "../userParameters/action";
import { setLoaderAuth } from "../loaders";

defaults.width = "350px";
defaults.delay = 2000;
defaultModules.set(PNotifyMobile, {});

export const requestSingIn = (payload) => async (dispatch) => {
  try {
    dispatch(setLoaderAuth(true));
    const response = await fetch(`${API}/auth/login`, {
      method: "POST",
      body: JSON.stringify(payload),
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
      },
    });

    const { status } = response;
    if (status === 500 || status === 401 || status === 404) {
      setTimeout(() => dispatch(setLoaderAuth(false)), 5000);
      error({
        text: "Incorrect E-mail or password.",
      });
      return;
    }
    const { accessToken, user } = await response.json();
    if (!user) {
      setTimeout(() => dispatch(setLoaderAuth(false)), 5000);
      info({
        text: "Incorrect E-mail or password.",
      });
    }
    saveTokenToStorage({ accessToken, user: JSON.stringify(user) });
    dispatch(setUserData(user));
    dispatch(setUserToken(accessToken));
    setTimeout(() => dispatch(setLoaderAuth(false)), 5000);
  } catch (error) {
    setTimeout(() => dispatch(setLoaderAuth(false)), 5000);
    info({
      text: error,
    });
  }
};

export const requestSingUp = (payload) => async (dispatch) => {
  try {
    dispatch(setLoaderAuth(true));
    const response = await fetch(`${API}/auth/registration`, {
      method: "POST",
      body: JSON.stringify(payload),
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    });

    const { status } = response;

    if (status === 500 || status === 401) {
      setTimeout(() => dispatch(setLoaderAuth(false)), 5000);
      error({
        text: "Incorrect E-mail, name or password.",
      });
      return;
    }

    if (status === 409) {
      setTimeout(() => dispatch(setLoaderAuth(false)), 5000);
      info({
        text: "This E-mail already exist!",
      });
      return;
    }

    setTimeout(() => dispatch(setLoaderAuth(false)), 5000);
    info({
      text: `Registration successfully!`,
    });
    return status;
  } catch (error) {
    setTimeout(() => dispatch(setLoaderAuth(false)), 5000);
    info({
      text: error,
    });
  }
};

export const getTokenFromStorage = () => (dispatch) => {
  try {
    const token = localStorage.getItem("accessToken");
    const user = JSON.parse(localStorage.getItem("user"));

    dispatch(setUserToken(token));
    dispatch(setUserData(user));
  } catch (error) {
    console.log(error);
  }
};

export const logout = () => async (dispatch) => {
  try {
    const response = await fetch(`${API}/auth/logout`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("accessToken")}`,
      },
    });
    const accessToken = null;
    const user = null;
    const userParameters = {
      dailyRate: null,
      notAllowedProducts: [],
      bloodGroup: null,
    };
    dispatch(setUserToken(accessToken));
    dispatch(setUserData(user));
    dispatch(setUserParameters(userParameters));
    saveTokenToStorage({ accessToken, user: JSON.stringify(user) });
  } catch (error) {
    console.log(error);
  }
};

export const saveTokenToStorage = ({ user, accessToken }) => {
  localStorage.setItem("user", user);
  localStorage.setItem("accessToken", accessToken);
};
