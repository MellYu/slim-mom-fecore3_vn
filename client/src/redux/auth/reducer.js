import { createReducer } from "@reduxjs/toolkit";
import { SET_USER_TOKEN, SET_USER_DATA } from "./types";

const innitialState = {
  accessToken: null,
  user: {},
};

export const authReducer = createReducer(innitialState, {
  [SET_USER_TOKEN]: (state, action) => ({
    ...state,
    accessToken: action.payload,
  }),
  [SET_USER_DATA]: (state, action) => ({ ...state, user: action.payload }),
});
