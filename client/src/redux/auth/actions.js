import { createAction } from "@reduxjs/toolkit";
import { SET_USER_TOKEN, SET_USER_DATA } from "./types";

export const setUserToken = createAction(SET_USER_TOKEN);
export const setUserData = createAction(SET_USER_DATA);
