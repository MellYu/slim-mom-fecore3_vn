export const getUserisAuthenticate = (state) => Boolean(state.auth.accessToken);
export const getUserData = (state) => state.auth.user;
export const getUserName = (state) => state.auth.user.name;
export const getUserId = (state) => state.auth.user.id;
