import { combineReducers } from "redux";

import { authReducer } from "./auth";
import { diaryDataReducer } from "./diaryData";
import { loadersReducer } from "./loaders";
import { productsForSelect } from "./productsForSelect";
import { userParametersReducer } from "./userParameters";
import { themeReducer } from "./theme";

export const rootReducer = combineReducers({
  auth: authReducer,
  diaryData: diaryDataReducer,
  loaders: loadersReducer,
  productsForSelect,
  userParameters: userParametersReducer,
  themeReducer,
});
