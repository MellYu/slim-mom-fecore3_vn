import { createAction } from "@reduxjs/toolkit";
import {
  SET_LOADER_AUTH,
  SET_LOADER_PRODUCTS_FOR_SELECT,
  SET_LOADER_PRODUCTS_LIST,
  SET_LOADER_USER_PARAMETERS,
} from "./types";

export const setLoaderAuth = createAction(SET_LOADER_AUTH);
export const setLoaderProductsForSelect = createAction(
  SET_LOADER_PRODUCTS_FOR_SELECT
);
export const setLoaderProductsList = createAction(SET_LOADER_PRODUCTS_LIST);
export const setLoaderUserParameters = createAction(SET_LOADER_USER_PARAMETERS);
