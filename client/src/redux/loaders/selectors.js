export const checkLoaders = (state) =>
  Object.values(state.loaders).some((elem) => elem === true);
