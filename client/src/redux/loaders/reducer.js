import { createReducer } from "@reduxjs/toolkit";

import {
  setLoaderAuth,
  setLoaderProductsForSelect,
  setLoaderProductsList,
  setLoaderUserParameters,
} from "./actions";

const initialState = {
  auth: false,
  productsForSelect: false,
  productsList: false,
  userParameters: false,
};

export const loadersReducer = createReducer(initialState, {
  [setLoaderAuth]: (state, { payload }) => ({ ...state, auth: payload }),
  [setLoaderProductsForSelect]: (state, { payload }) => ({
    ...state,
    productsForSelect: payload,
  }),
  [setLoaderProductsList]: (state, { payload }) => ({
    ...state,
    productsList: payload,
  }),
  [setLoaderUserParameters]: (state, { payload }) => ({
    ...state,
    userParameters: payload,
  }),
});
