import { SET_THEME } from "./types";

const innitialState = {
  theme: 'lightTheme'
};

export const themeReducer = (state = innitialState, action) => {
  switch (action.type) {
    case SET_THEME:
      return { ...state, theme: action.payload };
    default:
      return state;
  }
};
