import { createAction } from "@reduxjs/toolkit";
import { SET_PRODUCTS_FOR_SELECT } from "./types";

export const setProductsForSelect = createAction(SET_PRODUCTS_FOR_SELECT);
