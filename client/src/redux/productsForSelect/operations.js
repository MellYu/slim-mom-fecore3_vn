import { setProductsForSelect } from "./actions";
import { setLoaderProductsForSelect } from "../loaders";

import { API } from "../../api";

export const fetchProductsForSelect = (action) => async (dispatch) => {
  try {
    dispatch(setLoaderProductsForSelect(true));
    const response = await fetch(`${API}/products`);
    const responseData = await response.json();
    const productsList = responseData.map(({ calories, titleUA }) => ({
      value: calories,
      label: titleUA,
    }));
    dispatch(setProductsForSelect(productsList));
    setTimeout(() => dispatch(setLoaderProductsForSelect(false)), 5000);
  } catch (error) {
    setTimeout(() => dispatch(setLoaderProductsForSelect(false)), 5000);
    console.log(error);
  }
};
