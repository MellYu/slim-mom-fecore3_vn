import { createReducer } from "@reduxjs/toolkit";
import { setProductsForSelect } from "./actions";

export const productsForSelect = createReducer(null, {
  [setProductsForSelect]: (state, { payload }) => payload,
});
