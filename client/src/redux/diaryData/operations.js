import {
  setProductsList,
  addProductToList,
  deleteProtuctFromList,
  setUsedCalories,
} from "./actions";
import { setLoaderProductsList } from "../loaders";
import { API } from "../../api";

export const requestProductsList = (payload) => async (dispatch) => {
  try {
    dispatch(setLoaderProductsList(true));
    const { currentDate, userId } = payload;
    const response = await fetch(`${API}/products/${userId}/${currentDate}`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("accessToken")}`,
      },
    });
    const currentDayInfo = await response.json();
    const usedCalories = currentDayInfo.products.reduce(
      (acc, value) => acc + value.calories,
      0
    );
    dispatch(setUsedCalories(usedCalories));
    dispatch(setProductsList(currentDayInfo));
    setTimeout(() => dispatch(setLoaderProductsList(false)), 5000);
  } catch (error) {
    setTimeout(() => dispatch(setLoaderProductsList(false)), 5000);
    console.log(error);
  }
};

export const addProductOperation = (payload) => async (dispatch) => {
  try {
    dispatch(setLoaderProductsList(true));
    const { currentDate, userId, productObject } = payload;
    const response = await fetch(`${API}/products/${userId}/${currentDate}`, {
      method: "POST",
      body: JSON.stringify(productObject),
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("accessToken")}`,
      },
    });
    const { addedProduct, currentDay } = await response.json();
    const usedCalories = currentDay.products.reduce(
      (acc, value) => acc + value.calories,
      0
    );
    dispatch(setUsedCalories(usedCalories));
    dispatch(addProductToList(addedProduct));
    setTimeout(() => dispatch(setLoaderProductsList(false)), 5000);
  } catch (error) {
    setTimeout(() => dispatch(setLoaderProductsList(false)), 5000);
    console.log(error);
  }
};

export const deleteProductOperation = (payload) => async (dispatch) => {
  try {
    dispatch(setLoaderProductsList(true));
    const { userId, currentDate, productId } = payload;
    const response = await fetch(
      `${API}/products/${userId}/${currentDate}/${productId}`,
      {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${localStorage.getItem("accessToken")}`,
          "Content-Type": "application/json",
        },
      }
    );
    const { currentDay } = await response.json();
    const usedCalories = currentDay.products.reduce(
      (acc, value) => acc + value.calories,
      0
    );
    dispatch(setUsedCalories(usedCalories));
    dispatch(deleteProtuctFromList(productId));
    setTimeout(() => dispatch(setLoaderProductsList(false)), 5000);
  } catch (error) {
    setTimeout(() => dispatch(setLoaderProductsList(false)), 5000);
    console.log(error);
  }
};
