import { createAction } from "@reduxjs/toolkit";
import {
  SET_USED_CALORIES,
  SET_CURRENT_DATE,
  SET_PRODUCTS_LIST,
  ADD_PRODUCT_TO_LIST,
  DELETE_PRODUCT_FROM_LIST,
} from "./types";

export const setUsedCalories = createAction(SET_USED_CALORIES);
export const setCurrentDate = createAction(SET_CURRENT_DATE);
export const setProductsList = createAction(SET_PRODUCTS_LIST);
export const addProductToList = createAction(ADD_PRODUCT_TO_LIST);
export const deleteProtuctFromList = createAction(DELETE_PRODUCT_FROM_LIST);
