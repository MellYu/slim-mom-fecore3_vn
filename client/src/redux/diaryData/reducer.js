import { createReducer } from "@reduxjs/toolkit";
import {
  addProductToList,
  setCurrentDate,
  setProductsList,
  setUsedCalories,
  deleteProtuctFromList,
} from "./actions";

const currentDate = new Date().getDate();
let currentMonth = new Date().getMonth();
currentMonth = currentMonth < 10 ? `0${currentMonth + 1}` : currentMonth + 1;
const currentYear = new Date().getFullYear();

const initialState = {
  currentDate: `${currentDate}-${currentMonth}-${currentYear}`,
  usedCalories: null,
  productsList: [],
};

export const diaryDataReducer = createReducer(initialState, {
  [setUsedCalories]: (state, action) => ({
    ...state,
    usedCalories: action.payload,
  }),
  [setCurrentDate]: (state, action) => ({
    ...state,
    currentDate: action.payload,
  }),
  [setProductsList]: (state, action) => ({
    ...state,
    productsList: action.payload.products,
  }),
  [addProductToList]: (state, action) => ({
    ...state,
    productsList: [...state.productsList, action.payload],
  }),
  [deleteProtuctFromList]: (state, { payload }) => ({
    ...state,
    productsList: [
      ...state.productsList.filter((item) => item._id !== payload),
    ],
  }),
});
