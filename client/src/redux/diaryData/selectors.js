export const getCurrentDate = (state) => state.diaryData.currentDate;
export const getUsedCalories = (state) => state.diaryData.usedCalories;
export const getProductsList = (state) => state.diaryData.productsList;
