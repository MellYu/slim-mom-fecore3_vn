import routes from "../routes";

export const navigation = {
  private: [
    {
      title: "ДНЕВНИК",
      link: routes.diary,
    },
    {
      title: "КАЛЬКУЛЯТОР",
      link: routes.calculator,
    },
  ],
  restricted: [
    {
      title: "ЛОГИН",
      link: routes.login,
    },
    {
      title: "РЕГИСТРАЦИЯ",
      link: routes.registration,
    },
  ],
};