import React from "react";
import styled from "styled-components";
import { SelectInput as SelectInputUI, Button as ButtonUI } from "../../../ui";

const InputContainer = styled.div`
  margin-top: 50px;
  display: flex;
  flex-direction: column;
  width: 100%;
  text-align: center;
`;
const SelectInput = styled(SelectInputUI)`
  margin-top: 20px;
  & .css-g1d714-ValueContainer {
    padding: 10px 0 !important;
  }
`;

const Button = styled(ButtonUI)`
  margin: 40px auto;
`;

export const Component = ({
  onAddProduct,
  setName,
  setGrams,
  setCalories,
  productsForSelect,
  grams,
  goBack,
}) => {
  const selectProductOnChangeHandler = (e) => {
    setName(e.label);
    setCalories(e.value);
  };
  const selectGramsOnChangeHandler = (e) => {
    setGrams(e.value);
  };
  const BtnOnClickHandler = () => {
    onAddProduct();
    goBack();
  };

  return (
    <InputContainer>
      <SelectInput
        options={productsForSelect}
        onChange={selectProductOnChangeHandler}
        placeholder={"Введите название продукта"}
      />
      <SelectInput
        placeholder={"Граммы"}
        options={grams}
        onChange={selectGramsOnChangeHandler}
      />
      <Button primary={+true} onClick={BtnOnClickHandler}>
        Добавить
      </Button>
    </InputContainer>
  );
};
