import React, { useState } from "react";
import { useDispatch, connect } from "react-redux";
import { useHistory } from "react-router-dom";

import { Component } from "./Component";
import { getProductsForSelect } from "../../../redux/productsForSelect";
import { getUserId } from "../../../redux/auth";
import { getCurrentDate, addProductOperation } from "../../../redux/diaryData";
import { grams as gramsData } from "../../../data/grams";

const Container = ({ productsForSelect, userId, currentDate }) => {
  const dispatch = useDispatch();
  const [name, setName] = useState("");
  const [grams, setGrams] = useState(0);
  const [calories, setCalories] = useState(0);

  const history = useHistory();
  const goBack = history.goBack;

  const onAddProduct = () => {
    const productObject = {
      titleUA: name,
      calories: (calories * grams) / 100,
      weight: grams,
    };
    dispatch(addProductOperation({ currentDate, userId, productObject }));
  };

  return (
    <Component
      onAddProduct={onAddProduct}
      setName={setName}
      setGrams={setGrams}
      setCalories={setCalories}
      productsForSelect={productsForSelect}
      grams={gramsData}
      goBack={goBack}
    />
  );
};

const mapStateToProps = (state) => ({
  productsForSelect: getProductsForSelect(state),
  userId: getUserId(state),
  currentDate: getCurrentDate(state),
});
export const AddProductPage = connect(mapStateToProps)(Container);
