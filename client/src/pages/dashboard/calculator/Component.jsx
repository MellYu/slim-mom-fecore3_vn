import React from "react";
import styled from "styled-components";

import { SpringCalendar } from "../../../ui";
import {
  CalculatorForm,
  CalculatorTitle as CalculatorTitleShared,
} from "../../../shared";

const CalculatorTitle = styled(CalculatorTitleShared)`
  margin-top: 30px;
`;

export const Component = ({ onClickModalRedirect }) => {
  return (
    <SpringCalendar>
      {(props) => (
        <div style={props}>
          <CalculatorTitle> Узнай свою суточную норму калорий </CalculatorTitle>
          <CalculatorForm onClickModalRedirect={onClickModalRedirect} />
        </div>
      )}
    </SpringCalendar>
  );
};
