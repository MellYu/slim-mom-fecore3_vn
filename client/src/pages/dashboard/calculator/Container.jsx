import React from "react";

import { useHistory } from "react-router-dom";
import routes from "../../../routes";

import { Component } from "./Component";

export const Container = () => {
  const history = useHistory();
  const modalHandlerOnClick = () => {
    history.push(routes.diary);
  };
  return <Component onClickModalRedirect={modalHandlerOnClick} />;
};
