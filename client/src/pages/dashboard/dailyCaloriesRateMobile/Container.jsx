import React from "react";
import { useHistory } from "react-router-dom";

import routes from "../../../routes";
import { Component } from "./Component";

export const Container = () => {
  const history = useHistory();
  const handlerComponentOnRedirect = () => {
    history.push(routes.diary);
  };
  return <Component onRedirect={handlerComponentOnRedirect} />;
};
