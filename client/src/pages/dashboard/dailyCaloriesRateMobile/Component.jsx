import React from "react";

import { DailyCaloriesRate } from "./../../../shared";

export const Component = ({ onRedirect }) => {
  return <DailyCaloriesRate onRedirect={onRedirect} />;
};
