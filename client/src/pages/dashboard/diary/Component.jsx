import { useMediaQuery } from "react-responsive";
import React from "react";
import styled from "styled-components";

import { DiaryProductsList } from "./diaryProductList";
import { DiaryDate } from "./DiaryDate";
import { SelectProduct } from "./SelectProduct";
import { AddButton as AddButtonUI, SpringGeneral } from "../../../ui";

const Container = styled.div`
  padding-top: 40px;
  display: block;
`;

const AddButton = styled(AddButtonUI)`
  display: block;
  margin: 60px auto 0;
`;

export const Component = ({
  date,
  setDate,
  onAddProduct,
  productList,
  productName,
  productGrams,
  addProductToList,
  deleteProductFromList,
}) => {
  const isMobile = useMediaQuery({ maxWidth: 380 });
  return (
    <SpringGeneral>
      {(props) => (
        <Container style={props}>
          <DiaryDate
            date={date}
            setDate={setDate}
          />
          {!isMobile && (
            <SelectProduct
              productName={productName}
              productGrams={productGrams}
              addProductToList={addProductToList}
            />
          )}
          <DiaryProductsList
            productList={productList}
            deleteProductFromList={deleteProductFromList}
          />
          {isMobile && <AddButton onClick={onAddProduct} />}
        </Container>
      )}
    </SpringGeneral>
  );
};
