import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { useDispatch, connect } from "react-redux";

import { Component } from "./Component";
import {
  fetchProductsForSelect,
  getProductsForSelect,
} from "../../../redux/productsForSelect";
import {
  getCurrentDate,
  setCurrentDate,
  requestProductsList,
  getProductsList,
  addProductOperation,
  deleteProductOperation,
} from "../../../redux/diaryData";
import { getUserId } from "../../../redux/auth";
import {
  getUserDailyRate,
  fetchUserParameters,
  getUserBloodGroup,
  setUserParametersOperation,
} from "../../../redux/userParameters";
import { setLoaderUserParameters } from "../../../redux/loaders";
import routes from "../../../routes";
import { grams } from "../../../data";

const Container = ({
  currentDate,
  userId,
  productsList,
  productsForSelect,
  userDailyRate,
  userBloodGroup,
}) => {
  const history = useHistory();
  const dispatch = useDispatch();

  useEffect(() => {
    if (!productsForSelect) dispatch(fetchProductsForSelect());
  }, []);

  useEffect(() => {
    if (userDailyRate === null) {
      return dispatch(fetchUserParameters(userId));
    }
    if (userDailyRate) {
      return dispatch(
        setUserParametersOperation({ userDailyRate, userBloodGroup, userId })
      );
    }
    if (userDailyRate === 0) {
      dispatch(setLoaderUserParameters(false));
      return history.push(routes.calculator);
    }
  }, [userDailyRate]);

  useEffect(() => dispatch(requestProductsList({ currentDate, userId })), [
    currentDate,
  ]);

  const setDate = (date) => {
    dispatch(setCurrentDate(date));
  };

  const hendleAddProduct = () => {
    history.push(routes.addProduct);
  };

  const addProductToList = (name, calories, grams) => {
    const productObject = {
      titleUA: name,
      calories: (calories * grams) / 100,
      weight: grams,
    };
    dispatch(addProductOperation({ currentDate, userId, productObject }));
  };

  const deleteProductFromList = (productId) => {
    dispatch(deleteProductOperation({ currentDate, userId, productId }));
  };

  return (
    <Component
      date={currentDate.replace("-", ".").replace("-", ".")}
      setDate={setDate}
      productList={productsList}
      productName={productsForSelect}
      productGrams={grams}
      onAddProduct={hendleAddProduct}
      addProductToList={addProductToList}
      deleteProductFromList={deleteProductFromList}
    />
  );
};

const mapStateToProps = (state) => ({
  productsForSelect: getProductsForSelect(state),
  currentDate: getCurrentDate(state),
  userId: getUserId(state),
  productsList: getProductsList(state),
  userDailyRate: getUserDailyRate(state),
  userBloodGroup: getUserBloodGroup(state),
});

export const DiaryPage = connect(mapStateToProps)(Container);
