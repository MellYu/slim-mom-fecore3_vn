import React from "react";
import styled from "styled-components";

import { Calendar as CalendarUI, H1 as H1UI } from "../../../ui";

const HeaderContainer = styled.div`
  display: flex;
  flex-wrap: nowrap;
  align-items: center;
  @media only screen and (max-width: 380px) {
    justify-content: center;
  }
`;

const SelectedDay = styled(H1UI)`
  margin-right: 21px;
  color: ${({ theme }) => theme.colors.dark};
  @media only screen and (max-width: 768px) {
    font-size: 34px;
  }
  @media only screen and (max-width: 380px) {
    font-size: 18px;
  }
`;

const Calendar = styled(CalendarUI)`
  @media only screen and (max-width: 767px) {
    width: 300px;
    left: -200px;
    font-size: 12px;
  }
`;

export const DiaryDate = ({ date, setDate }) => {
  return (
    <HeaderContainer>
      <SelectedDay> {date} </SelectedDay>
      <Calendar date={date} setDate={setDate} />
    </HeaderContainer>
  );
};
