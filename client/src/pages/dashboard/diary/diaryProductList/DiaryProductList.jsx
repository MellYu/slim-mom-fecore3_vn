import React from "react";
import styled from "styled-components";
import { Scrollbars as ScrollbarsRS } from "react-custom-scrollbars";

import { DiaryProductListItem } from "./DiaryProductListItem";
import { DiaryEmptyList } from "./DiaryEmptyList";

const List = styled.ul`
  display: block;
  position: relative;
  width: 588px;
  height: 290px;
  overflow: hidden;
  margin-top: 40px;

  @media only screen and (max-width: 380px) {
    width: 290px;
    height: 240px;
    margin-left: auto;
    margin-right: auto;
  }
`;
const Scrollbars = styled(ScrollbarsRS)`
  max-height: 290px;
  @media only screen and (max-width: 380px) {
    height: 240px;
  }
`;
const Shadow = styled.div`
  position: absolute;
  margin: 0 auto;
  width: 99%;
  z-index: 100;
  height: 50px;
  background: linear-gradient(
    0deg,
    ${({ theme }) => theme.colors.gradient.visible} 0%,
    ${({ theme }) => theme.colors.gradient.unvisible} 100%
  );
  bottom: 0;
  left: 0;
`;

export const DiaryProductsList = ({ productList, deleteProductFromList }) => {
  return (
    <>
      {productList.length !== 0 ? (
        <List>
          <Shadow />
          <Scrollbars>
            {productList.map((product) => (
              <DiaryProductListItem
                {...product}
                key={product._id}
                deleteProductFromList={deleteProductFromList}
              />
            ))}
          </Scrollbars>
        </List>
      ) : (
        <DiaryEmptyList />
      )}
    </>
  );
};
