import React, { memo, useState } from "react";
import styled from "styled-components";

import {
  AppearanceOnLeft,
  CloseButton as CloseButtonUI,
  H4 as H4UA,
} from "../../../../ui";

const Item = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  width: 588px;
  min-height: 50px;
  max-height: 60px;
  font-size: 14px;
  @media only screen and (max-width: 380px) {
    width: 290px;
    height: auto;
  }
`;

const H4 = styled(H4UA)`
  padding: 20px 0px;
  margin-right: 30px;
  border-bottom: 1px solid ${({ theme }) => theme.colors.secondaryShade};
  font-weight: 400 !important;
  color: ${({ theme }) => theme.colors.primary};
  @media only screen and (max-width: 380px) {
    text-align: left;
    margin-right: 10px;
    padding-bottom: 10px;
  }
`;
const NameItem = styled(H4)`
  width: 240px;
  text-align: left;
  @media only screen and (max-width: 380px) {
    width: 137px;
  }
`;
const WeightItem = styled(H4)`
  width: 104px;
  text-align: right;
  @media only screen and (max-width: 380px) {
    width: 50px;
  }
`;
const CaloriesItem = styled(WeightItem)`
  @media only screen and (max-width: 380px) {
    width: 70px;
  }
`;
const CloseButton = styled(CloseButtonUI)`
  width: 50px;
  margin-right: 15px;
  @media only screen and (max-width: 380px) {
    width: 15px;
    margin-right: 10px;
    padding-left: 5px;
  }
`;
const Span = styled.span`
  margin-left: 4px;
  font-size: 14px;
  @media only screen and (max-width: 380px) {
    font-size: 9px;
  }
`;

const Component = ({
  titleUA,
  weight,
  calories,
  _id,
  deleteProductFromList,
}) => {
  const handlerBtnOnClick = (e) => {
    deleteProductFromList(_id);
  };
  return (
    <AppearanceOnLeft>
      {(props) => (
        <Item style={props}>
          <NameItem>{titleUA}</NameItem>
          <WeightItem>{weight} г</WeightItem>
          <CaloriesItem>
            {calories} <Span>ккал</Span>{" "}
          </CaloriesItem>
          <CloseButton onClick={handlerBtnOnClick} />
        </Item>
      )}
    </AppearanceOnLeft>
  );
};

export const DiaryProductListItem = memo(Component);
