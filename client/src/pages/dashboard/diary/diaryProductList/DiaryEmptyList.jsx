import React from "react";
import styled from "styled-components";

import { H2 } from "../../../../ui";

const Wrapper = styled.div`
  height: auto;
  margin-top: 40px;
  padding: 50px 20px;
  text-align: center;
  border-radius: 8px;
  border: 3px dashed ${({ theme }) => theme.colors.primary};
  @media only screen and (max-width: 380px) {
    border: 1px dashed ${({ theme }) => theme.colors.primary};
    padding: 20px 10px;
  }
`;
const Title = styled(H2)`
  color: ${({ theme }) => theme.colors.dark};
  @media only screen and (max-width: 380px) {
    font-size: 14px;
  }
`;

export const DiaryEmptyList = () => {
  return (
    <Wrapper>
      <Title>
        Здесь будет отображаться список употребленных вами продуктов
      </Title>
    </Wrapper>
  );
};
