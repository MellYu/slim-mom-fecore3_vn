import React, { useState } from "react";
import styled from "styled-components";

import {
  AddButton as AddButtonUI,
  SelectInputTextRight,
  VirtualizedSelect,
} from "../../../ui";

const InputContainer = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
`;
const LeftSelectInput = styled(VirtualizedSelect)`
  width: 240px;
  margin-right: 30px;
`;
const RightSelectInput = styled(SelectInputTextRight)`
  width: 104px;
  margin-right: 75px;
`;
const AddButton = styled(AddButtonUI)`
  width: 50px;
  margin-top: 10px;
`;

export const SelectProduct = ({
  productName,
  productGrams,
  addProductToList,
}) => {
  const [name, setName] = useState("");
  const [grams, setGrams] = useState(0);
  const [calories, setCalories] = useState(0);

  const selectName = (e) => {
    setName(e.label);
    setCalories(e.value);
  };

  const selectGrams = (e) => {
    setGrams(e.value);
  };
  const handlerBtnOnClick = (e) => {
    addProductToList(name, calories, grams);
  };
  return (
    <InputContainer>
      <LeftSelectInput
        placeholder={"Введите название продукта"}
        options={productName}
        onChange={selectName}
      />
      <RightSelectInput
        placeholder={"Граммы"}
        options={productGrams}
        onChange={selectGrams}
      />
      <AddButton onClick={handlerBtnOnClick} />
    </InputContainer>
  );
};
