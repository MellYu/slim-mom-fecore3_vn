import React from "react";
import styled from "styled-components";

import { SpringGeneral } from "../../ui";
import {
  CalculatorTitle as CalculatorTitleShared,
  CalculatorForm,
} from "../../shared";

const CalculatorTitle = styled(CalculatorTitleShared)`
  margin-top: 30px;
`;

export const Component = ({ onClickModalRedirect }) => {
  return (
    <SpringGeneral>
      {(props) => (
        <div style={props}>
          <CalculatorTitle>
            {" "}
            Просчитай свою суточную норму калорий прямо сейчас
          </CalculatorTitle>
          <CalculatorForm onClickModalRedirect={onClickModalRedirect} />
        </div>
      )}
    </SpringGeneral>
  );
};
