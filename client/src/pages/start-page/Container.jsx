import React from "react";
import { useHistory } from "react-router-dom";
import { navigation } from "../../data";

import { StartLayout } from "../../layouts";
import routes from "../../routes";
import { Component } from "./Component";

export const Container = () => {
  const history = useHistory();
  const handlerOnClickModalRedirect = () => {
    history.push(routes.registration);
  };
  return (
    <StartLayout
      menuList={navigation.restricted}
    >
      <Component onClickModalRedirect={handlerOnClickModalRedirect} />
    </StartLayout>
  );
};
