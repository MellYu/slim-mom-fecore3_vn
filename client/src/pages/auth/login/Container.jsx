import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import routes from "../../../routes";
import { requestSingIn } from "./../../../redux/auth";
import { emailHandler, blurHandler, passwordHandler } from "./../../../utils";

import { Component } from "./Component";

export const Container = () => {
  const history = useHistory();
  const dispatch = useDispatch();

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isEmailEmpty, setEmailEmpty] = useState(false);
  const [isPasswordEmpty, setPasswordEmpty] = useState(false);
  const [emailError, setEmailError] = useState("Недопустимый логин");
  const [passwordError, setPasswordError] = useState(
    "Введите пароль больше 8 символов"
  );

  const onLogin = () => {
    dispatch(requestSingIn({ email, password }));
  };
  const onRegistration = () => {
    history.push(routes.registration);
  };
  const onBlurComponentHandler = (e) => {
    blurHandler(setEmailEmpty, setPasswordEmpty, e.target.name);
  };
  const emailComponentHandler = (e) => {
    emailHandler(setEmail, setEmailError, e.target.value);
  };
  const passwordComponentHandler = (e) => {
    passwordHandler(setPassword, setPasswordError, e.target.value);
  };
  return (
    <Component
      onRegistration={onRegistration}
      onLogin={onLogin}
      emptyEmail={isEmailEmpty}
      errorEmail={emailError}
      emptyPassword={isPasswordEmpty}
      errorPassword={passwordError}
      onBlurHandler={onBlurComponentHandler}
      emailHandler={emailComponentHandler}
      passwordHandler={passwordComponentHandler}
    />
  );
};
