import React from "react";
import styled from "styled-components";
import { FormText } from "reactstrap";

import { Input as InputUI, Label as LabelUI, SpringGeneral } from "../../../ui";
import { FormGroup } from "../form";
import { Actions } from "../form";
import { Title } from "../Title";

const LabelLogin = styled(LabelUI)`
  width: 100%;
  margin-bottom: 20px;
`;

const LabelPassword = styled(LabelUI)`
  width: 100%;
  margin-bottom: 60px;
`;

const Input = styled(InputUI)`
  width: 100% !important;
`;
const Wrapper = styled.div`
  margin-top: 70px;
  @media only screen and (max-width: 768px) {
    margin-top: 91px;
  }
  @media only screen and (max-width: 380px) {
    margin-top: 40px;
  }
`;

const ErrorWrapper = styled(FormText)`
  margin: 5px 0px;
  padding: 0px;
  color: ${({ theme }) => theme.colors.danger} !important;
  text-align: center;
  font-size: 12px;
  text-decoration: underline;
`;
export const Component = ({
  onRegistration,
  onLogin,
  emptyEmail,
  emptyPassword,
  errorEmail,
  errorPassword,
  onBlurHandler,
  emailHandler,
  passwordHandler,
}) => {
  const handlerEmailInputOnClick = (e) => {
    emailHandler(e);
  };
  const handlerEmailInputOnBlur = (e) => {
    onBlurHandler(e);
  };
  const handlerPasswordInputOnClick = (e) => {
    passwordHandler(e);
  };
  const handlerPasswordInputOnBlur = (e) => {
    onBlurHandler(e);
  };

  return (
    <SpringGeneral>
      {(props) => (
        <Wrapper style={props}>
          <Title>ВХОД</Title>
          <FormGroup>
            <LabelLogin>
              <Input
                name="email"
                placeholder="Логин"
                type="email"
                onChange={handlerEmailInputOnClick}
                onBlur={handlerEmailInputOnBlur}
              />
              {emptyEmail && errorEmail && (
                <ErrorWrapper>{errorEmail}</ErrorWrapper>
              )}
            </LabelLogin>
            <LabelPassword>
              <Input
                name="password"
                placeholder="Пароль"
                type="password"
                onChange={handlerPasswordInputOnClick}
                onBlur={handlerPasswordInputOnBlur}
              />
              {emptyPassword && errorPassword && (
                <ErrorWrapper>{errorPassword}</ErrorWrapper>
              )}
            </LabelPassword>
            <Actions onRegistration={onRegistration} onLogin={onLogin} />
          </FormGroup>
        </Wrapper>
      )}
    </SpringGeneral>
  );
};
