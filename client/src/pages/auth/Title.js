import styled from "styled-components";

import { H4 } from "../../ui";


export const Title = styled(H4)`
color: ${({ theme }) => theme.colors.primary};
margin-bottom: 51px;
@media only screen and (max-width: 380px) {
  text-align: center;
  width: 100%;
}
`