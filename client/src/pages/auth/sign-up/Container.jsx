import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import routes from "../../../routes";
import {
  emailHandler,
  blurHandler,
  passwordHandler,
  nameHandler,
} from "./../../../utils";

import { requestSingUp } from "./../../../redux/auth";

import { Component } from "./Component";

export const Container = () => {
  const history = useHistory();
  const dispatch = useDispatch();

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [name, setName] = useState("");
  const [isNameEmpty, setNameEmpty] = useState(false);
  const [isEmailEmpty, setEmailEmpty] = useState(false);
  const [isPasswordEmpty, setPasswordEmpty] = useState(false);
  const [nameError, setNameError] = useState("Введите имя");
  const [emailError, setEmailError] = useState("Недопустимый логин");
  const [passwordError, setPasswordError] = useState(
    "Введите пароль больше 8 символов"
  );

  const onRegistration = () => {
    dispatch(requestSingUp({ email, password, name })).then((response) => {
      if (response) history.push(routes.login);
    });
  };
  const onLogin = () => {
    history.push(routes.login);
  };
  const nameComponentHandler = (e) => {
    nameHandler(setName, setNameError, e.target.value);
  };
  const onBlurComponentHandler = (e) => {
    blurHandler(setEmailEmpty, setPasswordEmpty, e.target.name, setNameEmpty);
  };
  const emailComponentHandler = (e) => {
    emailHandler(setEmail, setEmailError, e.target.value);
  };
  const passwordComponentHandler = (e) => {
    passwordHandler(setPassword, setPasswordError, e.target.value);
  };

  return (
    <Component
      onRegistration={onRegistration}
      isNameEmpty={isNameEmpty}
      nameError={nameError}
      emptyEmail={isEmailEmpty}
      errorEmail={emailError}
      emptyPassword={isPasswordEmpty}
      errorPassword={passwordError}
      nameHandler={nameComponentHandler}
      onBlurHandler={onBlurComponentHandler}
      emailHandler={emailComponentHandler}
      passwordHandler={passwordComponentHandler}
      onLogin={onLogin}
    />
  );
};
