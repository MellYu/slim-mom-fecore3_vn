import React from "react";
import styled from "styled-components";
import { FormText } from "reactstrap";

import { Input as InputUI, Label as LabelUI, SpringGeneral } from "../../../ui";
import { FormGroup } from "../form";
import { Actions } from "../form";
import { Title } from "../Title";

const Label = styled(LabelUI)`
  width: 100%;
`;

const Input = styled(InputUI)`
  margin-bottom: 20px;
  @media only screen and (max-width: 380px) {
    width: 100% !important;
  }
`;
const PasswordLabel = styled(Label)`
  margin-bottom: 60px;
`;
const Wrapper = styled.div`
  margin-top: -48px;
  @media only screen and (max-width: 768px) {
    margin-top: 57px;
  }
  @media only screen and (max-width: 380px) {
    margin-top: 40px;
  }
`;
const ErrorWrapper = styled(FormText)`
  margin: 5px 0px;
  padding: 0px;
  color: ${({ theme }) => theme.colors.danger} !important;
  text-align: center;
  font-size: 12px;
  text-decoration: underline;
`;

export const Component = ({
  onRegistration,
  onLogin,
  emptyEmail,
  emptyPassword,
  errorEmail,
  errorPassword,
  onBlurHandler,
  emailHandler,
  passwordHandler,
  nameHandler,
}) => {
  const nameInputOnChangeHandler = (e) => {
    nameHandler(e);
  };
  const emailInputOnChangeHandler = (e) => {
    emailHandler(e);
  };
  const emailInputOnBlurHandler = (e) => {
    onBlurHandler(e);
  };
  const passwordInputOnChangeHandler = (e) => {
    passwordHandler(e);
  };
  const passwordInputOnBlurHandler = (e) => {
    onBlurHandler(e);
  };
  return (
    <SpringGeneral>
      {(props) => (
        <Wrapper style={props}>
          <Title>РЕГИСТРАЦИЯ</Title>
          <FormGroup>
            <Label>
              <Input
                name="name"
                placeholder="Имя"
                onChange={nameInputOnChangeHandler}
              />
            </Label>
            <Label>
              <Input
                name="email"
                placeholder="Логин"
                type="email"
                onChange={emailInputOnChangeHandler}
                onBlur={emailInputOnBlurHandler}
              />
              {emptyEmail && errorEmail && (
                <ErrorWrapper>{errorEmail}</ErrorWrapper>
              )}
            </Label>
            <PasswordLabel>
              <Input
                name="password"
                placeholder="Пароль"
                type="password"
                onChange={passwordInputOnChangeHandler}
                onBlur={passwordInputOnBlurHandler}
              />
              {emptyPassword && errorPassword && (
                <ErrorWrapper>{errorPassword}</ErrorWrapper>
              )}
            </PasswordLabel>
            <Actions onRegistration={onRegistration} onLogin={onLogin} />
          </FormGroup>
        </Wrapper>
      )}
    </SpringGeneral>
  );
};
