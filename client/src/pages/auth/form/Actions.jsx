import React from "react";
import styled from "styled-components";

import { Button } from "../../../ui";

const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  @media only screen and (max-width: 380px) {
    flex-direction: column;
    align-items: center;
  }
`;

const PrimaryButton = styled(Button)`
  @media only screen and (max-width: 380px) {
    margin-bottom: 20px;
  }
`

export const Actions = ({ onLogin, onRegistration }) => {
  return (
    <Wrapper>
      <PrimaryButton onClick={onLogin} primary={+true}>
        Вход
      </PrimaryButton>
      <Button onClick={onRegistration}>Регистрация</Button>
    </Wrapper>
  );
};
