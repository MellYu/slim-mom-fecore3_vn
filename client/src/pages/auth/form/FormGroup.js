import { FormGroup as FormGroupRS } from 'reactstrap'
import styled from 'styled-components'

export const FormGroup = styled(FormGroupRS)`
  width: 382px;
  @media only screen and (max-width: 380px) {
    width: 100%;
  }
  @media only screen and (max-width: 768px) {
    margin-bottom: 200px;
  }
`