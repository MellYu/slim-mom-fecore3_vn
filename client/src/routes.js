// eslint-disable-next-line import/no-anonymous-default-export
export default {
  home: "/",
  restricted: "/auth",
  login: "/auth/login",
  registration: "/auth/sign-up",
  private: "/dashboard",
  diary: "/dashboard/diary",
  calculator: "/dashboard/calculator",
  addProduct: "/dashboard/add-product",
  dailyCaloriesRate: "/dashboard/daily-calories-rate",
};
