import styled from "styled-components";

import { SpringGeneral } from "../../../ui";
import arr from "./images";
import { ReactComponent as Backdrop } from "./images/backdrop.svg";

export const ImgAbsolute = styled.img`
  z-index: -1;
  position: absolute;
`;

const BackdropSvg = styled(Backdrop)`
  z-index: -1;
  position: absolute;
  & path {
    fill: ${({ theme }) => theme.colors.secondaryShade2};
  }
  @media (min-width: 768px) {
    right: -150px;
    bottom: 0;
  }
  @media (min-width: 1280px) {
    top: 0;
    right: 0;
  } ;
`;

const Fruit = styled(ImgAbsolute).attrs(() => ({
  src: arr["fruit"],
}))`
  @media (min-width: 768px) {
    right: 0;
    bottom: 0;
  }
  @media (min-width: 1280px) {
    top: 0;
  } ;
`;

export const Background = () => {
  return (
    <>
      <SpringGeneral>
        {(props) => (
          <div style={props}>
            <BackdropSvg />
            <Fruit />
          </div>
        )}
      </SpringGeneral>
    </>
  );
};
