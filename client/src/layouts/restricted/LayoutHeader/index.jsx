import React from "react";
import { useMediaQuery } from "react-responsive";
import styled from "styled-components";

import { LayoutHeader as SharedLayoutHeader, NavMenu } from "../../../shared";
import { SelectTheme as SelectThemeUI, SpringGeneral } from "../../../ui";

const SelectTheme = styled(SelectThemeUI)`
  @media only screen and (min-width: 576px) {
    margin-left: 50px;
  }
`;

export const LayoutHeader = ({ menuList }) => {
  const isTablet = useMediaQuery({ maxWidth: 1279 });
  const isMobile = useMediaQuery({ maxWidth: 767 });

  return (
    <SharedLayoutHeader>
      {isTablet && <NavMenu list={menuList} />}
      {!isMobile && (
        <SpringGeneral>
          {(props) => (
            <div style={props}>
              <SelectTheme />
            </div>
          )}
        </SpringGeneral>
      )}
    </SharedLayoutHeader>
  );
};
