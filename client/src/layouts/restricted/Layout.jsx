import React from "react";
import styled from "styled-components";

import { Background } from "./Background";
import { LayoutBody as SharedLayoutBody } from "../../shared";
import { LayoutHeader } from "./LayoutHeader";
import { useMediaQuery } from "react-responsive";
import { SelectTheme, SpringGeneral } from "../../ui";

const LayoutBody = styled(SharedLayoutBody)`
  min-height: 100%;
  max-height: 100%;
  height: 100%;
`;

export const Layout = ({ menuList, children }) => {
  const isTablet = useMediaQuery({ minWidth: 768 });

  return (
    <>
      {isTablet && <Background />}
      <LayoutHeader menuList={menuList} />
      <LayoutBody>
        {children}
        {!isTablet && (
          <SpringGeneral>
            {(props) => (
              <div style={props}>
                <SelectTheme />
              </div>
            )}
          </SpringGeneral>
        )}
      </LayoutBody>
    </>
  );
};
