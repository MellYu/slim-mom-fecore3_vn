import React from "react";
import styled from "styled-components";
import { useMediaQuery } from "react-responsive";

import { Background } from "./Background";
import { LayoutHeader } from "./LayoutHeader";
import { LayoutBody } from "../../shared";
import { SelectTheme as SelectThemeUI, SpringGeneral } from "../../ui";

const SelectTheme = styled(SelectThemeUI)`
  @media only screen and (max-width: 1279px) {
    margin-top: 100px;
  }
`;

export const Layout = ({ menuList, children }) => {
  const isTablet = useMediaQuery({ minWidth: 768 });

  return (
    <>
      {isTablet && <Background />}
      <LayoutHeader menuList={menuList} />
      <LayoutBody>
        {children}
        <SpringGeneral>
          {(props) => (
            <div style={props}>
              <SelectTheme menuPlacement="top" />
            </div>
          )}
        </SpringGeneral>
      </LayoutBody>
    </>
  );
};
