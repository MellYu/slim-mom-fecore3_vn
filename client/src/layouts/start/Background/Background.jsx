import styled from "styled-components";

import arr from "./images";
import { ReactComponent as Backdrop } from "./images/backdrop.svg";
import { SpringGeneral } from '../../../ui';

export const ImgAbsolute = styled.img`
  z-index: -1;
  position: absolute;
`;

const BackdropSvg = styled(Backdrop)`
  z-index: -1;
  position: absolute;
  & path {
    fill: ${({ theme }) => theme.colors.secondaryShade2};
  }
  @media (min-width: 768px) {
    right: 0;
    top: 330px;
  }
  @media (min-width: 1280px) {
    top: auto;
    bottom: 0;
  } ;
`;

const BananImg = styled(ImgAbsolute).attrs(() => ({
  src: arr["banan"],
}))`
  @media (min-width: 768px) {
    right: 0;
    top: 690px;
  }
  @media (min-width: 1280px) {
    top: 0;
  } ;
`;

const Strawberry = styled(ImgAbsolute).attrs(() => ({
  src: arr["strawberry"],
}))`
  @media (min-width: 768px) {
    right: 0;
    top: 470px;
  }
  @media (min-width: 1280px) {
    bottom: 0;
  } ;
`;

const GrassImg = styled(ImgAbsolute).attrs(() => ({
  src: arr["grass"],
}))`
  @media (min-width: 768px) {
    right: 0;
    top: 340px;
    transform: rotate(90deg);
  }
  @media (min-width: 1280px) {
    transform: rotate(0);
    top: 0;
  } ;
`;

export const Background = () => {
  return (
    <SpringGeneral>
      {(props) => (
        <div style={props}>
          <BackdropSvg />
          <BananImg />
          <Strawberry />
          <GrassImg />
        </div>
      )}
    </SpringGeneral>
  );
};
