import banan from "./banan.png";
import grass from "./grass.png";
import strawberry from "./strawberry.png";

export default {
  banan,
  grass,
  strawberry
};
