import React from "react";

import { LayoutHeader as SharedLayoutHeader, NavMenu } from "../../../shared";

export const LayoutHeader = ({ menuList }) => {
  return (
    <SharedLayoutHeader>
      <NavMenu list={menuList} />
    </SharedLayoutHeader>
  );
};
