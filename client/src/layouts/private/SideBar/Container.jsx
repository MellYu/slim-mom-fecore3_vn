import React from "react";
import { connect } from "react-redux";
import {
  getUserDailyRate,
  getUserNotAllowedProducts,
} from "../../../redux/userParameters";
import { getUsedCalories, getCurrentDate } from "../../../redux/diaryData";
import { Component } from "./Component";

const Container = ({
  userDailyRate,
  userNotAllowedProducts,
  usedCalories,
  currentDate,
}) => {
  return (
    <Component
      currentDate={currentDate.replace("-", ".").replace("-", ".")}
      residueCarories={userDailyRate - usedCalories}
      used={usedCalories}
      dailyConsumption={userDailyRate}
      rate={userDailyRate !== 0 ? (usedCalories * 100) / userDailyRate : 0}
      deprecatedProducts={userNotAllowedProducts}
    />
  );
};

const mapStateToProps = (state) => ({
  userDailyRate: getUserDailyRate(state),
  userNotAllowedProducts: getUserNotAllowedProducts(state),
  usedCalories: getUsedCalories(state),
  currentDate: getCurrentDate(state),
});

export const SideBar = connect(mapStateToProps)(Container);
