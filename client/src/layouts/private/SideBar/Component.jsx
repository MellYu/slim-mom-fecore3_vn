import React from "react";
import styled from "styled-components";
import { Col, Row } from "reactstrap";
import { useMediaQuery } from "react-responsive";

import { Logout } from "../../../shared";
import { Summary } from "./Summary";
import { DeprecatedProducts } from "./DeprecatedProducts";
import grassDesktop from "./images/grassDesktop.png";
import grassTablet from "./images/grassTablet.png";
import { SelectTheme, SpringGeneral } from "../../../ui";

const LogoutWrapper = styled.div`
  @media screen and (min-width: 1280px) {
    padding-top: 114px;
    padding-bottom: 160px;
    display: flex;
    justify-content: space-between;
  }
`;

const SelectThemeForTablet = styled(SelectTheme)`
  margin: 20px 0 0 auto;
`;

const Wrapper = styled.div`
  height: 100%;
  background-color: ${({ theme }) => theme.colors.secondaryShade3};
  max-width: 100vw;
  padding: 40px 15px 84px;
  position: relative;
  @media screen and (min-width: 768px) {
    padding: 91px 87px;
    background-image: url(${grassTablet});
    background-repeat: no-repeat;
    background-position: right;
  }
  @media screen and (min-width: 1280px) {
    padding: 0 115px 0 90px;
    background-position: right;
    background-image: url(${grassDesktop});
  }
`;

const DeprecatedProductsWithMargin = styled(DeprecatedProducts)`
  padding-bottom: 20px;
  @media screen and (min-width: 1280px) {
    margin-top: 40px;
  }
  padding-bottom: 20px;
  @media screen and (max-width: 380px) {
    margin-top: 40px;
  }
`;

export const Component = ({
  currentDate,
  residueCarories,
  used,
  dailyConsumption,
  rate,
  deprecatedProducts,
}) => {
  const isDesktop = useMediaQuery({ minWidth: 1280 });

  return (
    <Wrapper>
      {isDesktop && (
        <SpringGeneral>
          {(props) => (
            <LogoutWrapper style={props}>
              <SelectTheme />
              <Logout />
            </LogoutWrapper>
          )}
        </SpringGeneral>
      )}
      <Row>
        <Col md={6} xl={12}>
          <Summary
            currentDate={currentDate}
            residueCarories={residueCarories}
            used={used}
            dailyConsumption={dailyConsumption}
            rate={rate}
            deprecatedProducts={deprecatedProducts}
          />
        </Col>
        <Col md={6} xl={12}>
          <DeprecatedProductsWithMargin
            deprecatedProducts={deprecatedProducts}
          />
        </Col>
        {!isDesktop && (
          <SpringGeneral>
            {(props) => (
              <Col xs={12} style={props}>
                <SelectThemeForTablet menuPlacement="top" />
              </Col>
            )}
          </SpringGeneral>
        )}
      </Row>
    </Wrapper>
  );
};
