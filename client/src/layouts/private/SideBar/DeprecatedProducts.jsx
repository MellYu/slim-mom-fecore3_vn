import React from "react";
import styled from "styled-components";
import { P as PUI, H4 as H4UI, AppearanceOnRight } from "../../../ui";

const ProhibitedProducts = styled.span`
  font-style: normal;
  font-weight: 400;
  color: ${({ theme }) => theme.colors.secondary};
`;
const H4 = styled(H4UI)`
  padding-bottom: 20px;
  margin: 0 auto;
  color: ${({ theme }) => theme.colors.dark};
  @media screen and (min-width: 768px) {
    padding-top: 0;
    padding-bottom: 40px;
  }
`;
const P = styled(PUI)`
  display: flex;
  justify-content: space-between;
  font-weight: 400;
  color: ${({ theme }) => theme.colors.secondary};
`;
const DeprecatedProductsWrapper = styled.div`
  width: 270px;
  padding-bottom: 60px;
  @media screen and (max-width: 380px) {
    margin-left: auto;
    margin-right: auto;
    text-align: center;
  }
`;

export const DeprecatedProducts = ({ deprecatedProducts = [], className }) => {
  return (
    <AppearanceOnRight>
      {(props) => (
        <DeprecatedProductsWrapper className={className} style={props}>
          <H4>Нерекомендуемые продукты</H4>
          {deprecatedProducts ? (
            deprecatedProducts.map((item, i) => (
              <ProhibitedProducts key={i}>{item}, </ProhibitedProducts>
            ))
          ) : (
            <P>Здесь будет отображаться Ваш рацион</P>
          )}
        </DeprecatedProductsWrapper>
      )}
    </AppearanceOnRight>
  );
};
