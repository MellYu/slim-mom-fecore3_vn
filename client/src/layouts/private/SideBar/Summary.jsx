import React from "react";
import styled from "styled-components";

import { P as PUI, H4 as H4UI, AppearanceOnLeft } from "../../../ui";

const H4 = styled(H4UI)`
  padding-top: 40px;
  padding-bottom: 20px;
  color: ${({ theme }) => theme.colors.dark};
  margin: 0 auto;
  @media screen and (min-width: 768px) {
    padding-top: 0;
    padding-bottom: 40px;
  }
`;
const P = styled(PUI)`
  display: flex;
  justify-content: space-between;
  font-weight: 400;
  color: ${({ theme }) => theme.colors.secondary};
`;
const Item = styled.li`
  list-style: none;
  margin-bottom: 10px;
`;
const Statistic = styled.ul`
  margin: 0;
`;
const Wrapper = styled.div`
  width: 200px;
  @media screen and (min-width: 768px) {
    width: 252px;
    &:not(:last-child) {
      margin-right: 60px;
    }
  }
  @media screen and (min-width: 1280px) {
    width: 270px;
    padding-bottom: 60px;
  }
  @media screen and (max-width: 380px) {
    margin-left: auto;
    margin-right: auto;
    text-align: center;
  }
`;

export const Summary = ({
  currentDate,
  residueCarories,
  used,
  dailyConsumption,
  rate,
}) => {
  return (
    <AppearanceOnLeft>
      {(props) => (
        <Wrapper style={props}>
          <H4>Сводка за {currentDate}</H4>
          <Statistic>
            <Item>
              <P>
                <span>Осталось</span>
                <span>{residueCarories ? residueCarories : 0} ккал</span>
              </P>
            </Item>
            <Item>
              <P>
                <span>Употреблено</span>
                <span>{used ? used : 0} ккал</span>
              </P>
            </Item>
            <Item>
              <P>
                <span>Дневная норма</span>
                <span>{dailyConsumption ? dailyConsumption : 0} ккал</span>
              </P>
            </Item>
            <Item>
              <P>
                <span>n% от нормы</span>
                <span>{rate.toFixed(2)}%</span>
              </P>
            </Item>
          </Statistic>
        </Wrapper>
      )}
    </AppearanceOnLeft>
  );
};
