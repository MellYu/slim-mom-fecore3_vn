import React, { useState } from "react";
import { Col as ColRS, Row as RowRS } from "reactstrap";
import styled from "styled-components";

import { LayoutBody } from "../../shared";
import { LayoutHeader } from "./LayoutHeader";
import { SideBar } from "./SideBar";
import { BurgerMenu } from "../../ui";

const Row = styled(RowRS)`
  margin: 0;
  min-height: 100vh;
  overflow: hidden;
`;
const Col = styled(ColRS)`
  padding: 0;
`;

export const Layout = ({ menuList, children }) => {
  const [burgerToggle, setBurgerToggle] = useState(false);

  return (
    <>
      {burgerToggle && (
        <BurgerMenu menuList={menuList} setBurgerToggle={setBurgerToggle} />
      )}
      <Row>
        <Col xl={7}>
          <LayoutHeader
            menuList={menuList}
            burgerToggle={burgerToggle}
            setBurgerToggle={setBurgerToggle}
          />
          <LayoutBody>{children}</LayoutBody>
        </Col>
        <Col xl={5}>
          <SideBar />
        </Col>
      </Row>
    </>
  );
};
