import React from "react";
import { useMediaQuery } from "react-responsive";
import { useHistory } from "react-router-dom";
import styled from "styled-components";

import {
  LayoutHeader as SharedLayoutHeader,
  Logout as SharedLogout,
  LogoutWrapper,
  NavMenu,
} from "../../../shared";
import {
  BurgerIcon,
  GoBackButton,
  SelectTheme,
  SpringGeneral,
} from "../../../ui";

const HeaderInterfaseWrapper = styled.div`
  display: flex;
  align-items: center;
`;
const Logout = styled(SharedLogout)`
  margin-left: 50px;
`;

export const LayoutHeader = ({ menuList, burgerToggle, setBurgerToggle }) => {
  const isTablet = useMediaQuery({ maxWidth: 768 });
  const isMobile = useMediaQuery({ maxWidth: 380 });

  const history = useHistory();
  const goBack = history.goBack;

  return (
    <>
      <SharedLayoutHeader>
        {isTablet ? (
          <SpringGeneral>
            {(props) => (
              <HeaderInterfaseWrapper style={props}>
                <SelectTheme />
                {!isMobile && <Logout />}
                <BurgerIcon
                  burgerToggle={burgerToggle}
                  setBurgerToggle={setBurgerToggle}
                />
              </HeaderInterfaseWrapper>
            )}
          </SpringGeneral>
        ) : (
          <NavMenu list={menuList} />
        )}
      </SharedLayoutHeader>
      {isMobile && (
        <SpringGeneral>
          {(props) => (
            <div style={props}>
              <LogoutWrapper style={props}>
                <GoBackButton onClick={goBack} />
                <Logout />
              </LogoutWrapper>
            </div>
          )}
        </SpringGeneral>
      )}
    </>
  );
};
