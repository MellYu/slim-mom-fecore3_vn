import { useMediaQuery } from "react-responsive";
import styled from "styled-components";

import { Logo } from "./Logo";

const Head = styled.header`
  padding-top: 82px;
  padding-left: 115px;
  @media (min-width: 1280px) {
    padding-bottom: 143px;
  }
  @media only screen and (max-width: 768px) {
    padding: 0 87px;
  }
  @media only screen and (max-width: 380px) {
    padding: 0 15px;
  }
`;

const Wrapper = styled.div`
  display: flex;
  align-items: flex-end;
  position: relative;
  @media (max-width: 1279px) {
    margin-bottom: 19px;
    padding-top: 18px;
    justify-content: space-between;
    align-items: center;
  }
`;

const Line = styled.hr`
  display: block;
  height: 2px;
  border: none;
  background: ${({ theme: { colors } }) => colors.secondaryShade};
`;

export const Header = ({ children }) => {
  const isTablet = useMediaQuery({ maxWidth: 1279 });

  return (
    <>
      <Head>
        <Wrapper>
          <Logo />
          {children}
        </Wrapper>
      </Head>
      {isTablet && <Line />}
    </>
  );
};
