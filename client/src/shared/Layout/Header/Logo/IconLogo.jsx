import { useMediaQuery } from "react-responsive";
import styled from "styled-components";

import bigLogo from "./images/bigLogo.png";
import smallLogo from "./images/logo.png";
import { LogoText } from "./LogoText";

const Wrapper = styled.div`
  position: relative;
  @media screen and (min-width: 1280px) {
    width: 170px;
  }
`;
const StyledLogoText = styled(LogoText)`
  @media screen and (max-width: 380px) {
    display: none;
  }
  @media screen and (max-width: 1280px) {
    margin-left: 5px;
  }
  @media screen and (min-width: 1280px) {
    position: absolute;
    right: 10px;
    bottom: 0;
  }
`;

const Logo = styled.img.attrs(({ name }) => ({
  src: name === "smallLogo" ? smallLogo : bigLogo,
}))``;

export const IconLogo = ({ className, name }) => {
  const isMobile = useMediaQuery({ maxWidth: 767 });

  return (
    <Wrapper className={className}>
      {isMobile ? <Logo name="smallLogo" /> : <Logo />}
      <StyledLogoText />
    </Wrapper>
  );
};
