import styled from "styled-components";

import { ReactComponent as Mom } from "./images/Mom.svg";
import { ReactComponent as Slim } from "./images/Slim.svg";

const SlimSegment = styled(Slim)`
  & path {
    fill: ${({ theme }) => theme.colors.dark};
  }
`;

const MomSegment = styled(Mom)`
  & path {
    fill: ${({ theme }) => theme.colors.primary};
  }
`;

export const LogoText = ({ className }) => {
  return (
    <span className={className}>
      <SlimSegment />
      <MomSegment />
    </span>
  );
};
