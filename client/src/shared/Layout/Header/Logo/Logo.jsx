import { useMediaQuery } from "react-responsive";
import styled from "styled-components";

import { IconLogo } from "./IconLogo";
import { SpringGeneral } from "../../../../ui";

const DesctopLogo = styled(IconLogo)`
  margin-right: 18px;
`;

export const Logo = () => {
  const isTablet = useMediaQuery({ maxWidth: 1279 });
  const isDesctop = useMediaQuery({ minWidth: 1280 });

  return (
    <SpringGeneral>
      {(props) => (
        <div style={props}>
          {isDesctop && <DesctopLogo name="bigLogo" />}
          {isTablet && <IconLogo name="mediumLogo" />}
        </div>
      )}
    </SpringGeneral>
  );
};
