import styled from "styled-components";

export const Body = styled.div`
  padding-left: 115px;
  padding-right: 150px;
  min-height: 560px;
  max-height: 100vh;
  height: 100%;
  @media only screen and (max-width: 768px) {
    padding: 100px 87px 63px;
  }
  @media only screen and (max-width: 380px) {
    padding: 0 15px;
  }
`;
