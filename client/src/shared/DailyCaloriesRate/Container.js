import { connect } from "react-redux";

import { Component } from "./Component";
import {
  getUserDailyRate,
  getUserNotAllowedProducts,
} from "../../redux/userParameters";

const Container = ({
  toggle,
  modal,
  dailyRate,
  onRedirect,
  notAllowedProducts,
}) => {
  return (
    <Component
      calories={dailyRate}
      productList={notAllowedProducts}
      toggle={toggle}
      modal={modal}
      onRedirect={onRedirect}
    />
  );
};

const mapStateToProps = (state) => ({
  dailyRate: getUserDailyRate(state),
  notAllowedProducts: getUserNotAllowedProducts(state),
});

export const DailyCaloriesRate = connect(mapStateToProps)(Container);
