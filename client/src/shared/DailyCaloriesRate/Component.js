import React from "react";
import styled from "styled-components";

import {
  H2,
  CaloriesValue as CaloriesValueUI,
  H4,
  CaloriesSpan as CaloriesSpanUI,
  Button as ButtonUI,
  SpringGeneral,
} from "../../ui";

const Wrapper = styled.div`
  margin: 0 "auto";
  padding: 0px;
  display: "flex";
  flex-direction: "column";
  align-items: "center";
  text-align: center;
`;

const Title = styled(H2)`
  color: ${({ theme }) => theme.colors.primary};
  @media screen and (min-width: 320px) {
    text-align: start;
    margin: 0px;
    margin-bottom: 40px;
  }
  @media screen and (min-width: 768px) {
    text-align: center;
    margin-bottom: 22px;
  }
`;

const DescriptionsContainer = styled.div`
  @media screen and (min-width: 320px) {
    width: 290px;
    margin: 0 auto;
    padding: 0;
    text-align: start;
  }

  @media screen and (min-width: 768px) {
    width: 330px;
    margin: 0 auto;
  }
`;

const CaloriesValue = styled(CaloriesValueUI)`
  color: ${({ theme }) => theme.colors.dark};
  padding-bottom: 30px;
  position: relative;

  @media screen and (min-width: 320px) {
    text-align: center;
    margin: 0 auto;
    padding-bottom: 20px;
    margin-bottom: 20px;
    border-bottom: 1px solid ${({ theme }) => theme.colors.secondaryShade};
  }

  @media screen and (min-width: 768px) {
    margin-bottom: 12px;
    padding-bottom: 12px;
  }
`;

const CaloriesSpan = styled(CaloriesSpanUI)`
  color: ${({ theme }) => theme.colors.dark};
`;
const RecomendPhrase = styled(H4)`
  color: ${({ theme }) => theme.colors.primary};
  @media screen and (min-width: 320px) {
    margin-bottom: 22px;

    @media screen and (min-width: 768px) {
      width: 330px;
    }
  }
`;

const ProductList = styled.ol`
  @media screen and (min-width: 320px) {
    padding: 0px 15px;
    text-align: start;
    color: ${({ theme }) => theme.colors.secondary};
    margin-bottom: 40px;
  }
`;

const Product = styled.li`
  margin-bottom: 10px;
`;

const Button = styled(ButtonUI)`
  margin: 0 0 80px 0;
  justify-content: center;
  border: none;
`;

export const Component = ({ calories, productList, toggle, onRedirect }) => {
  const handlerBtnOnClick = () => {
    toggle && toggle();
    onRedirect();
  };
  return (
    <SpringGeneral>
      {(props) => (
        <Wrapper style={props}>
          <Title>Ваша рекомендуемая суточная норма калорий составляет</Title>
          <DescriptionsContainer>
            <CaloriesValue>
              {calories} <CaloriesSpan>ккал</CaloriesSpan>
            </CaloriesValue>
            <RecomendPhrase>
              Продукты которые вам не рекомендуется употреблять
            </RecomendPhrase>
            <ProductList>
              {productList.map((product, i) => (
                <Product key={i}>{product}</Product>
              ))}
            </ProductList>
          </DescriptionsContainer>
          <Button primary={+true} onClick={handlerBtnOnClick}>
            Начать худеть
          </Button>
        </Wrapper>
      )}
    </SpringGeneral>
  );
};
