import styled from "styled-components";

import { SpringGeneral } from "../../ui";
import { Link } from "./Link";

export const NavList = styled.nav`
  list-style: none;
  padding-left: 22px;
  display: inline-flex;
  align-items: center;
  min-height: 32px;
  border-left: 2px solid ${({ theme: { colors } }) => colors.secondaryShade};
  @media (max-width: 1279px) {
    border: none;
  }
`;

export const NavMenu = ({ list }) => {
  return (
    <SpringGeneral>
      {(props) => (
        <NavList style={props}>
          {list.map((item, i) => (
            <Link link={item.link} title={item.title} key={i} />
          ))}
        </NavList>
      )}
    </SpringGeneral>
  );
};
