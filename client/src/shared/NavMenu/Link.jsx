import { NavLink } from "react-router-dom";
import styled from "styled-components";

export const Link = styled(NavLink).attrs(({ link, title }) => ({
  to: link,
  children: title,
}))`
  text-decoration: none;
  color: ${({ theme }) => theme.colors.secondary};
  font-size: 14px;
  margin-right: 22px;
  font-weight: 700;
  &.active {
    color: ${({ theme }) => theme.colors.dark};
  }
  &:hover {
    color: ${({ theme }) => theme.colors.primary};
    text-decoration: none;
  }
`;
