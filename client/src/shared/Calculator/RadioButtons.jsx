import React from "react";
import { Label } from "reactstrap";
import styled from "styled-components";
import { RadioButton, RadioButtonGroup } from "../../ui";

const RadioButtonWrapper = styled.div`
  display: flex;
  width: 239px;
  justify-content: space-between;
  padding-left: 5px;
`;
const BLOOD_GROUP = ["1", "2", "3", "4"];

export const RadioButtons = ({ formDataSet }) => {
  return (
    <RadioButtonWrapper>
      <RadioButtonGroup>
        {BLOOD_GROUP.map((item, i) => (
          <Label key={i}>
            <RadioButton
              value={item}
              name="bloodGroup"
              formDataSet={formDataSet}
            />
          </Label>
        ))}
      </RadioButtonGroup>
    </RadioButtonWrapper>
  );
};
