import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { useMediaQuery } from "react-responsive";
import { useDispatch } from "react-redux";

import { Component } from "./Component";
import { calculateUserParametersmeters } from "../../redux/userParameters";
import routes from "../../routes";

let formData;

export const Container = ({ onClickModalRedirect }) => {
  const [modal, setModal] = useState(false);
  const [isDisabled, setIsDisabled] = useState(true);
  const isDailyCaloriesRateOpen = () => setModal(!modal);
  const dispatch = useDispatch();

  const isDesktopOrTablet = useMediaQuery({ minWidth: 321 });

  const history = useHistory();
  const goToMobilePage = () => {
    history.push(routes.dailyCaloriesRate);
  };

  useEffect(() => {
    formData = new FormData();
  }, []);

  const formDataSet = ({ target: { name, value } }) => {
    formData.set(name, value);
    if (
      formData.has("height") &&
      formData.has("age") &&
      formData.has("weight") &&
      formData.has("desiredWeight") &&
      formData.has("bloodGroup")
    )
      setIsDisabled(false);
  };

  const sendFormData = () => {
    let object = {};
    formData.forEach((value, key) => {
      object[key] = value;
    });
    dispatch(calculateUserParametersmeters(object));
  };

  return (
    <Component
      isDailyCaloriesRateOpen={isDailyCaloriesRateOpen}
      modal={modal}
      isDesktopOrTablet={isDesktopOrTablet}
      dailyCaloriesRateMobile={goToMobilePage}
      formDataSet={formDataSet}
      sendFormData={sendFormData}
      onClickModalRedirect={onClickModalRedirect}
      isDisabled={isDisabled}
    />
  );
};
