import styled from "styled-components";

import { H1 } from "../../ui";

export const Title = styled(H1)`
  color: ${({ theme }) => theme.colors.darkShade};
  display: block;
  text-align: left;
  width: 605px;
  margin-bottom:48px;

  @media only screen and (max-width: 768px) {
    margin-bottom:40px;
  }
  @media only screen and (max-width: 380px) {
    width: 290px;
    margin-bottom:20px;
  }
`;
