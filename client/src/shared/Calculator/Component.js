import styled from "styled-components";
import { Col, Form as FormRS, FormGroup as FormGroupRS, Row } from "reactstrap";

import {
  Button as ButtonUI,
  Input as InputUI,
  Label as LabelUI,
  RadioButtonTitle as RadioButtonTitleUI,
  Modal,
} from "../../ui";
import { DailyCaloriesRate } from "../DailyCaloriesRate";
import { RadioButtons } from "./RadioButtons";

const Form = styled(FormRS)`
  display: block;
  width: 522px;
  text-align: left;
  @media only screen and (max-width: 768px) {
    width: 520px;
  }
  @media only screen and (max-width: 380px) {
    width: 100%;
  }
`;

const RadioButtonTitle = styled(RadioButtonTitleUI)`
  @media only screen and (max-width: 380px) {
    padding: 10px 0;
  }
`;

const FormGroup = styled(FormGroupRS)`
  display: block;
  margin-bottom: 45px;
  @media only screen and (max-width: 768px) {
    margin-bottom: 40px;
  }
  @media only screen and (max-width: 380px) {
    width: 240px;
    margin-bottom: 40px;
  }
`;

const Input = styled(InputUI)`
  @media only screen and (max-width: 380px) {
    height: 36px !important;
    margin-bottom: 20px;
  }
`;

const Title = styled(RadioButtonTitle)`
  padding: 20px 5px;
  @media only screen and (max-width: 380px) {
    padding: 10x 5px;
  }
`;

const Label = styled(LabelUI)`
  @media only screen and (max-width: 380px) {
    margin-bottom: 0px;
  }
`;

const Button = styled(ButtonUI)`
  margin-left: 346px;
  @media only screen and (max-width: 768px) {
    margin-left: 0px;
  }
  @media only screen and (max-width: 380px) {
    margin: 0 auto;
  }
`;

export const Component = ({
  isDailyCaloriesRateOpen,
  modal,
  isDesktopOrTablet,
  dailyCaloriesRateMobile,
  formDataSet,
  sendFormData,
  onClickModalRedirect,
  isDisabled,
}) => {
  const handlerBtnOnClick = () => {
    sendFormData();
    isDesktopOrTablet ? isDailyCaloriesRateOpen() : dailyCaloriesRateMobile();
  };
  return (
    <Form>
      <FormGroup>
        <Row>
          <Col xs={12} md={6} xl={6}>
            <Label>
              <Input placeholder="Рост" name="height" onChange={formDataSet} />
            </Label>

            <Label>
              <Input placeholder="Возраст" name="age" onChange={formDataSet} />
            </Label>

            <Label>
              <Input
                placeholder="Текущий вес"
                name="weight"
                onChange={formDataSet}
              />
            </Label>
          </Col>

          <Col xs={12} md={6} xl={6}>
            <Label>
              <Input
                placeholder="Желаемый вес"
                name="desiredWeight"
                onChange={formDataSet}
              />
            </Label>
            <Title>Группа крови </Title>
            <RadioButtons formDataSet={formDataSet} />
          </Col>
        </Row>
      </FormGroup>

      <Button
        primary={+true}
        disabled={isDisabled}
        onClick={handlerBtnOnClick}
        block
      >
        Похудеть
      </Button>
      {isDesktopOrTablet && (
        <Modal toggle={isDailyCaloriesRateOpen} modal={modal}>
          <DailyCaloriesRate
            modal={modal}
            toggle={isDailyCaloriesRateOpen}
            onRedirect={onClickModalRedirect}
          />
        </Modal>
      )}
    </Form>
  );
};
