import styled from "styled-components";

const Wrapper = styled.div`
  height: 40px;
  margin-bottom: 40px;
  background-color: ${({ theme }) => theme.colors.secondaryShade3};
`;

const Body = styled.div`
  padding-left: 15px;
  padding-right: 15px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 100%;
`;

export const LogoutWrapper = ({ children, style }) => {
  return (
    <Wrapper style={style}>
      <Body>{children}</Body>
    </Wrapper>
  );
};
