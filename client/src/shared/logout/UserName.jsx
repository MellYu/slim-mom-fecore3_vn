import styled from "styled-components";
import { P } from "../../ui";



export const UserName = styled(P).attrs(({ name }) => ({
  children: name,
}))`
  display: flex;
  align-items: center;
  padding-right: 15px;
  margin-right: 15px;
  max-height: 32px;
  border-right: 2px solid ${({ theme }) => theme.colors.secondaryShade};
  color: ${({ theme }) => theme.colors.dark};
  @media (min-width: 1279px) {
    padding-right: 22px;
    margin-right: 22px;
  }
`;
