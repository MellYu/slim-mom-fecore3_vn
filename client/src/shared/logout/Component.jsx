import { useDispatch } from "react-redux";
import styled from "styled-components";
import React, { useEffect } from "react";

import { UserName } from "./UserName";
import { LogoutButton } from "./LogoutButton";
import { setUserData } from "./../../redux/auth/";

const Wrapper = styled.div`
  display: flex;
`;

export const Component = ({ userName, onLogout, className }) => {
  const dispatch = useDispatch();
  const user = JSON.parse(localStorage.getItem("user"));

  useEffect(() => {
    if (user) {
      dispatch(setUserData(user));
    }
  });

  return (
    <Wrapper className={className}>
      <UserName name={userName} />
      <LogoutButton onLogout={onLogout} />
    </Wrapper>
  );
};
