import styled from "styled-components";
import { P } from "../../ui";

const Button = styled.button`
  border: none;
  padding: 0;
  color: ${({ theme: { colors } }) => colors.secondary};
  background: inherit;
  height: 32px;
`;

export const LogoutButton = ({ onLogout }) => (
  <Button onClick={onLogout}>
    <P>Выйти</P>
  </Button>
);
