import React from "react";
import { connect, useDispatch } from "react-redux";
import { getUserName, logout } from "../../redux/auth";
import { Component } from "./Component";

const Container = ({ userName, className }) => {
  const dispatch = useDispatch();

  const onLogout = () => {
    dispatch(logout());
  };

  return (
    <Component onLogout={onLogout} userName={userName} className={className} />
  );
};

const mapStateToProps = (state) => ({
  userName: getUserName(state),
});

export const Logout = connect(mapStateToProps)(Container);
