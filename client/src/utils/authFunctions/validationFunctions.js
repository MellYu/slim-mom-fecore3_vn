export const emailHandler = ( setEmail, setEmailError, value) => {
  setEmail(value);
  const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  if (!re.test(String(value).toLowerCase())) {
    setEmailError("Некоректный email");
  } else setEmailError("");
};
export const blurHandler = (
  setEmailEmpty,
  setPasswordEmpty,
  name,
  setNameEmpty
) => {
  switch (name) {
    case "email":
      setEmailEmpty(true);
      break;
    case "password":
      setPasswordEmpty(true);
      break;
    case "name":
      setNameEmpty(true);
      break;
  }
};

export const nameHandler = (setName, setNameError, value) => {
  setName(value);
  if (value.length === 0) {
    setNameError("Введите имя");
  }
};

export const passwordHandler = (setPassword, setPasswordError, value) => {
  setPassword(value);
  if (value.length < 8) {
    setPasswordError("Длинна пароля должна быть меньше 8 символов!");
    if (!value) {
      setPasswordError("Введите пароль больше 8 символов");
    }
  } else setPasswordError("");
};
