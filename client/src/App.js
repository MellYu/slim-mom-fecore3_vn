import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { lazy, useEffect, Suspense } from "react";
import { ThemeProvider } from "styled-components";
import { connect, useDispatch } from "react-redux";

import { GlobalStyle, LoaderSpiner, PrivateRoute, RestrictedRoute } from "./ui";
import routes from "./routes";
import { PrivateLayout, RestrictedLayout } from "./layouts";
import { setUserData, setUserToken } from "./redux/auth";
import { checkLoaders } from "./redux/loaders";
import { darkTheme, lightTheme } from "./theme";
import { setTheme, themeSelector } from "./redux/theme";
import { navigation } from "./data";

const LoginPage = lazy(() =>
  import("./pages/auth/login").then((module) => ({ default: module.LoginPage }))
);
const SignUpPage = lazy(() =>
  import("./pages/auth/sign-up").then((module) => ({
    default: module.SignUpPage,
  }))
);
const StartPage = lazy(() =>
  import("./pages/start-page").then((module) => ({ default: module.StartPage }))
);
const CalcuratorPage = lazy(() =>
  import("./pages/dashboard/calculator").then((module) => ({
    default: module.CalcuratorPage,
  }))
);
const DiaryPage = lazy(() =>
  import("./pages/dashboard/diary").then((module) => ({
    default: module.DiaryPage,
  }))
);
const AddProductPage = lazy(() =>
  import("./pages/dashboard/add-product").then((module) => ({
    default: module.AddProductPage,
  }))
);
const DailyCaloriesRateMobile = lazy(() =>
  import("./pages/dashboard/dailyCaloriesRateMobile").then((module) => ({
    default: module.DailyCaloriesRateMobile,
  }))
);
const UIKit = lazy(() =>
  import("./ui").then((module) => ({ default: module.UIKit }))
);

const Application = ({ theme, isLoading }) => {
  const dispatch = useDispatch();
  
  const user = JSON.parse(localStorage.getItem("user"));
  const userToken = localStorage.getItem("accessToken");
  const themeToLocalStorage = localStorage.getItem("theme");
  useEffect(() => {
    if (user) {
      dispatch(setUserData(user));
      dispatch(setUserToken(userToken));
    }
    if (theme === "lightTheme") {
      dispatch(setTheme(themeToLocalStorage));
    }
  }, [user, theme, dispatch, themeToLocalStorage, userToken]);

  return (
    <ThemeProvider theme={theme === "lightTheme" ? lightTheme : darkTheme}>
      <Suspense fallback={<LoaderSpiner />}>
        <Router>
          <GlobalStyle />
          <Switch>
            <Route path="/uikit">
              <UIKit />
            </Route>
            <RestrictedRoute
              exact
              path={routes.home}
              redirect={routes.diary}
              component={<StartPage />}
            />
            <RestrictedRoute
              path={routes.restricted}
              redirect={routes.diary}
              component={
                <RestrictedLayout menuList={navigation.restricted}>
                  <Route path={routes.login} exact>
                    {isLoading ? <LoaderSpiner background /> : <LoginPage />}
                  </Route>
                  <Route path={routes.registration} exact>
                    {isLoading ? <LoaderSpiner background /> : <SignUpPage />}
                  </Route>
                  <Route path={routes.home} exact></Route>
                </RestrictedLayout>
              }
            />
            <PrivateRoute
              path={routes.private}
              component={
                <PrivateLayout menuList={navigation.private}>
                  <Route path={routes.calculator} exact>
                    <CalcuratorPage />
                  </Route>
                  <Route path={routes.diary} exact>
                    <DiaryPage />
                  </Route>
                  <Route path={routes.addProduct} exact>
                    {isLoading ? <LoaderSpiner /> : <AddProductPage />}
                  </Route>
                  <Route path={routes.dailyCaloriesRate} exact>
                    {isLoading ? <LoaderSpiner /> : <DailyCaloriesRateMobile />}
                  </Route>
                </PrivateLayout>
              }
            />
          </Switch>
        </Router>
      </Suspense>
    </ThemeProvider>
  );
};

const mapStateToProps = (state) => ({
  theme: themeSelector(state),
  isLoading: checkLoaders(state),
});

export const App = connect(mapStateToProps)(Application);
