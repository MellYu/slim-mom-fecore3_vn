const express = require("express");
const cors = require("cors");
const morgan = require("morgan");
const dotenv = require("dotenv");
const mongoose = require("mongoose");
const authRouter = require("./auth/auth.routers");
const productsRouter = require("./products/products.routers");
const userRouter = require("./users/user.routers");
const SwaggerUI = require("swagger-ui-express");
const swaggerDocument = require("./swagger/swagger.json");

dotenv.config();

const DB_NAME = process.env.DB_NAME;
const DB_PASSWORD = process.env.DB_PASSWORD;
const PORT = process.env.PORT || 3000;
const MONGO_URL = `mongodb+srv://UserAdmin:${DB_PASSWORD}@cluster0.dovi8.mongodb.net/${DB_NAME}?retryWrites=true&w=majority`;
let server;

start();

function start() {
  initServer();
  initMiddlewares();
  initRoutes();
  connectDatabase();
  listen();
}

// server.options("/:userId/:day/:productId", cors()); // enable pre-flight request for DELETE request
// server.del("/:userId/:day/", function () {});

function initServer() {
  server = express();
}

function initMiddlewares() {
  server.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
    res.header(
      "Access-Control-Allow-Headers",
      "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    );
    next();
  });
  server.use(express.json());
  server.use(cors());

  server.use(morgan("dev"));
}

function initRoutes() {
  server.use("/auth", authRouter);
  server.use("/products", productsRouter);
  server.use("/users", userRouter);
  server.use("/api-docs", SwaggerUI.serve, SwaggerUI.setup(swaggerDocument));
}

async function connectDatabase(req, res) {
  try {
    const connectDB = await mongoose.connect(MONGO_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false,
    });
    if (connectDB) {
      console.log("Database connection successful");
    } else {
      return res.status(500).send("Not connect db");
    }
  } catch (error) {
    console.log(error);
    process.exit(1);
  }
}

function listen() {
  server.listen(PORT, () => {
    console.log("Server is listening on port: ", PORT);
  });
}
