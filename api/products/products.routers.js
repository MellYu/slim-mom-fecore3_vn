const { Router } = require("express");

const productsController = require("./products.controllers");
const authController = require("../auth/auth.controllers");

const productsRouter = Router();

productsRouter.get("/", productsController.getTitleProducts);
productsRouter.post("/calculator", productsController.publicCalculator);
productsRouter.get(
  "/:userId/:day",
  authController.authorize,
  productsController.getProductPerDay
);
productsRouter.post(
  "/:userId/:day",
  authController.authorize,
  productsController.addProduct
);
productsRouter.delete(
  "/:userId/:day/:productId",
  authController.authorize,
  productsController.deleteProduct
);

module.exports = productsRouter;
