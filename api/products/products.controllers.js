const Product = require("./products.model");
const User = require("../users/user.model");

async function getTitleProducts(req, res) {
  Product.find({}).then((productsArr) => {
    const titleArray = productsArr.map(
      ({ title: { ua: titleUA }, weight, calories }) => ({
        titleUA,
        weight,
        calories,
      })
    );
    res.status(200).send(titleArray);
  });
}

async function calculeteData(data) {
  const { weight, height, age, desiredWeight, bloodGroup } = data;
  const dailyRate =
    10 * weight + 6.25 * height - 5 * age - 161 - 10 * (weight - desiredWeight);
  const userData = {
    weight,
    height,
    age,
    desiredWeight,
    bloodGroup,
    dailyRate,
  };

  const allProduct = await Product.find({});
  const notAllowedProducts = allProduct.filter(
    (product) => product.groupBloodNotAllowed[bloodGroup] === true
  );
  return { userData, dailyRate, notAllowedProducts, bloodGroup };
}

async function publicCalculator(req, res) {
  const { dailyRate, notAllowedProducts, bloodGroup } = await calculeteData(
    req.body
  );

  res
    .status(200)
    .json({ dailyRate, notAllowedProducts, bloodType: bloodGroup });
}

async function getProductPerDay(req, res) {
  const { userId, day } = req.params;
  const userData = await User.findById(userId);
  const currentDay = await checkDay(userData, day);
  res.json(currentDay);
}

async function addProduct(req, res) {
  const { userId, day } = req.params;
  const userData = await User.findById(userId);
  const currentDay = await checkDay(userData, day);
  currentDay.products.push(req.body);
  await userData.save();
  const lastProductLength = currentDay.products.length;
  const addedProduct = currentDay.products[lastProductLength - 1];
  res.json({ addedProduct, currentDay });
}

async function deleteProduct(req, res) {
  const { userId, day, productId } = req.params;
  const userData = await User.findById(userId);
  const currentDay = await checkDay(userData, day);
  currentDay.products = currentDay.products.filter(
    (product) => product._id.toString() !== productId
  );
  userData.save();
  res.json({ productId, currentDay });
}

async function checkDay(userData, day) {
  const findDay = userData.days.find((userDay) => {
    return userDay.data === day;
  });

  if (!findDay) {
    userData.days.push({ data: day, products: [] });
    await userData.save();
    return userData.days[userData.days.length - 1];
  }

  return findDay;
}
module.exports = {
  getTitleProducts,
  publicCalculator,
  getProductPerDay,
  addProduct,
  deleteProduct,
};
