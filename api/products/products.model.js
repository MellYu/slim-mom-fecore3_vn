const mongoose = require("mongoose");
const { Schema } = require("mongoose");

const productSchema = new Schema({
  categories: { type: Array, required: true },
  weight: { type: Number, required: true },
  title: { type: Object, required: true },
  calories: { type: Number, required: true },
  groupBloodNotAllowed: { type: Array, required: true },
});

const productModel = mongoose.model("Product", productSchema);

module.exports = productModel;
