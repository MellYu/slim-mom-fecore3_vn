const { Router } = require("express");

const AuthController = require("./auth.controllers");
const AuthValidators = require("./auth.validators");

const authRouter = Router();

authRouter.post(
  "/registration",
  AuthValidators.validateCreateUser,
  AuthController.createUser
);

authRouter.post(
  "/login",
  AuthValidators.validateSingIn,
  AuthValidators.validateUserId,
  AuthController.loginUser
);

authRouter.post("/logout", AuthController.authorize, AuthController.logoutUser);

authRouter.get("/refreshToken", AuthController.refreshToken);

module.exports = authRouter;
