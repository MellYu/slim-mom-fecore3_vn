const {
  Types: { ObjectId },
} = require("mongoose");
const Joi = require("joi");

const User = require("../users/user.model");

function validateCreateUser(req, res, next) {
  try {
    const validationRules = Joi.object({
      name: Joi.string().required(),
      email: Joi.string()
        .email({
          minDomainSegments: 2,
          tlds: { allow: ["com", "net"] },
        })
        .required(),
      password: Joi.string().required(),
    });

    const validationResult = validationRules.validate(req.body);
    if (validationResult.error) {
      const err = new Error(
        `Incorrect ${validationResult.error.details[0].context.label}`
      );
      err.code = 401;
      throw err;
    }

    next();
  } catch (error) {
    next(error);
  }
}

function validateSingIn(req, res, next) {
  try {
    const validationRules = Joi.object({
      email: Joi.string()
        .email({ minDomainSegments: 2, tlds: { allow: ["com", "net"] } })
        .required(),
      password: Joi.string().required(),
    });

    const validationResult = validationRules.validate(req.body);

    if (validationResult.error) {
      const err = new Error(
        `Incorrect  ${validationResult.error.details[0].context.label}`
      );
      err.code = 401;
      throw err;
    }

    next();
  } catch (error) {
    next(error);
  }
}

async function validateUserId(req, res, next) {
  try {
    const {
      body: { email },
    } = req;

    const user = await User.findOne({
      email,
    });

    if (user === null) {
      return res.status(404).send("User is not found");
    }

    if (!ObjectId.isValid(user._id)) {
      return res.status(401).send("Id is not valid");
    }
    req.user = user;
    next();
  } catch (error) {
    next(error);
  }
}

module.exports = {
  validateCreateUser,
  validateSingIn,
  validateUserId,
};
