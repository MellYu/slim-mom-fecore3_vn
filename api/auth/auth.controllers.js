const bcryptjs = require("bcryptjs");
const jwt = require("jsonwebtoken");

const User = require("../users/user.model");

async function createUser(req, res, next) {
  try {
    const {
      body: { email, password },
    } = req;

    const findEmail = await User.findOne({ email });

    if (findEmail) {
      return res.status(409).send("User with such an email exists");
    }

    const hashedPassword = await bcryptjs.hash(password, 10);
    const data = await User.create({
      ...req.body,
      password: hashedPassword,
    });

    res.status(201).json({ name: data.name, email: data.email });
  } catch (error) {
    next(error);
  }
}

async function loginUser(req, res) {
  const user = req.user;
  const {
    body: { email, password },
  } = req;

  const passwordCheckResult = await bcryptjs.compare(password, user.password);

  if (!passwordCheckResult) {
    return res.status(401).send("Email or password is wrong");
  }

  const userAccessToken = await jwt.sign(
    {
      userId: user._id,
    },
    process.env.JWT_ACCESS_SECRET,
    { expiresIn: "1d" }
  );

  const userRefreshToken = await jwt.sign(
    {
      userId: user._id,
    },
    process.env.JWT_REFRESH_SECRET,
    { expiresIn: "3d" }
  );

  const data = await User.findByIdAndUpdate(
    user._id,
    {
      $set: {
        accessToken: userAccessToken,
        refreshToken: userRefreshToken,
      },
    },
    { new: true }
  );

  res.status(200).json({
    accessToken: data.accessToken,
    refreshToken: data.refreshToken,
    user: { name: data.name, email: data.email, id: user._id },
  });
}

async function authorize(req, res, next) {
  try {
    const authorizationHeader = req.get("Authorization");
    if (!authorizationHeader) {
      return res.status(403).send("Not authorized");
    }
    const token = authorizationHeader.replace("Bearer ", "");
    const { userId } = await jwt.verify(token, process.env.JWT_ACCESS_SECRET);
    const user = await User.findById(userId);
    if (user === null) {
      return res.status(403).send("Not authorized");
    }
    req.user = user;
    next();
  } catch (error) {
    return res.status(401).send(error.message);
  }
}

async function logoutUser(req, res) {
  const {
    user: { _id },
  } = req;

  const user = await User.findByIdAndUpdate(_id, {
    $set: {
      accessToken: "",
      refreshToken: "",
    },
  });

  if (!user) {
    res.status(404).send("Not Found");
  }

  res.status(204).send("No Content");
}

async function refreshToken(req, res) {
  try {
    const { refreshToken } = req.body;
    const { userId } = await jwt.verify(
      refreshToken,
      process.env.JWT_REFRESH_SECRET
    );

    const userAccessToken = await jwt.sign(
      {
        userId: userId,
      },
      process.env.JWT_ACCESS_SECRET,
      { expiresIn: "1d" }
    );
    const userRefreshToken = await jwt.sign(
      {
        userId: userId,
      },
      process.env.JWT_REFRESH_SECRET,
      { expiresIn: "3d" }
    );

    const data = await User.findByIdAndUpdate(
      userId,
      {
        $set: {
          accessToken: userAccessToken,
          refreshToken: userRefreshToken,
        },
      },
      { new: true }
    );

    res.status(200).json({
      accessToken: data.accessToken,
      refreshToken: data.refreshToken,
    });
  } catch (error) {
    return res.status(401).send(error.message);
  }
}

module.exports = {
  createUser,
  loginUser,
  logoutUser,
  authorize,
  refreshToken,
};
