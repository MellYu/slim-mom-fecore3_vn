const User = require("./user.model");
const Products = require("../products/products.model");

async function getUserParameters(req, res) {
  const { userId } = req.params;
  const user = await User.findById(userId);
  const { dailyRate, bloodType } = user;
  const allProduct = await Products.find({});
  const notAllowedProducts = allProduct.filter(
    (product) => product.groupBloodNotAllowed[bloodType] === true
  );
  notAllowedProducts.length = 4;
  res.status(200).json({
    dailyRate: dailyRate,
    notAllowedProducts: notAllowedProducts,
    bloodType,
  });
}

async function addUserParameters(req, res) {
  const { userId } = req.params;
  const { userDailyRate, userBloodGroup } = req.body;
  const user = await User.findByIdAndUpdate(userId, {
    $set: { bloodType: userBloodGroup, dailyRate: userDailyRate },
  });

  res.status(200).json({ userDailyRate, userBloodGroup });
}

module.exports = {
  getUserParameters,
  addUserParameters,
};
