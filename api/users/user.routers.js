const { Router } = require("express");

const userControllers = require("./user.controllers");
const authController = require("../auth/auth.controllers");

const userRouter = Router();

userRouter.get("/:userId", userControllers.getUserParameters);
userRouter.post(
  "/:userId",
  authController.authorize,
  userControllers.addUserParameters
);

module.exports = userRouter;
