const mongoose = require("mongoose");
const { Schema } = require("mongoose");

const userSchema = new Schema({
  name: { type: String, required: true },
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  accessToken: { type: String },
  refreshToken: { type: String },
  bloodType: { type: Number, enum: [1, 2, 3, 4], default: 1 },
  dailyRate: { type: Number, required: true, default: 0 },
  days: [
    {
      data: { type: String, required: true },
      products: [
        {
          titleUA: { type: String, required: true },
          weight: { type: Number, required: true },
          calories: { type: Number, required: true },
        },
      ],
    },
  ],
});

const userModel = mongoose.model("User", userSchema);

module.exports = userModel;
